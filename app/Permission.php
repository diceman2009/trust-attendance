<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * Roles relationship
     * 
     * @return Role
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }
}
