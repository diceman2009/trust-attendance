<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'attendances';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['wonde_id', 'date', 'session', 'employee', 'attendance_code', 'student', 'school'];

    /**
     * attendance code relationship
     * 
     * @return Students
     */
    public function code()
    {
        return $this->hasOne('App\AttendanceCode', 'wonde_id', 'attendance_code');
    }


    /**
     * Student relationship
     * 
     * @return Students
     */
    public function student()
    {
        return $this->hasOne('App\Student', 'wonde_id', 'student');
    }

    /**
     * queryscope
     * 
     * @param  $query
     * @return Builder
     */
    public function scopeAttendanceCodes($query)
    {
        return $query->distinct('attendance_code')->get();
    }
}
