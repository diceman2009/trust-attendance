<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * Permissions relationship
     * 
     * @return Permission
     */
    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }


    /**
     * Users relationship
     * 
     * @return User
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
