<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
	use SoftDeletes;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['wonde_id', 'username', 'upn', 'year', 'tutor', 'sex', 'forename', 'surname', 'ethnicity', 'school'];

    /**
     * Join forname and surname to make a fullname attribute
     * 
     * @return String
     */
    public function getFullNameAttribute()
    {
        return $this->attributes['forename'] .' '. $this->attributes['surname'];
    }

    /**
     * Attendance relationship
     * 
     * @return Attendance
     */
    public function attendance()
    {
        return $this->hasMany('App\Attendance', 'student', 'wonde_id');
    }

}

