<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ScheduleLog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'schedule_log';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'command', 'comment'];


    /**
     * Human readable created at attribute
     * 
     * @return string
     */
    public function getFormattedCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->toDayDateTimeString();
    }

    /**
     * Overrides the default carbon method for updated_at
     * 
     * @return string
     */
    public function getUpdatedAtAttribute()
    {
        return $this->attributes['updated_at'];
    }

    /**
     * Complete logs queryscope
     * 
     * @param  $query
     * @return Builder
     */
    public function scopeComplete($query)
    {
        return $query->where('comment', '!=', '')->where('comment', '!=', 'aborted')->orderBy('updated_at', 'desc');
    }

    /**
     * MIS Import
     * 
     * @param  $query
     * @return Builder
     */
    public function scopeMISImport($query)
    {
        return $query->whereIn('command', ['mis:import', 'mis:import --update']);
    }

    /**
     * MIS Import queryscope
     * 
     * @param  $query
     * @return Builder
     */
    public function scopeMISImportStudents($query)
    {
        return $query->whereIn('command', ['mis:import-students', 'mis:import-students --update']);
    }

    /**
     * MIS Import queryscope
     * 
     * @param  $query
     * @return Builder
     */
    public function scopeMISImportAttendanceCodes($query)
    {
        return $query->whereIn('command', ['mis:import-attendance-codes', 'mis:import-attendance-codes --update']);
    }

    /**
     * MIS Import queryscope
     * 
     * @param  $query
     * @return Builder
     */
    public function scopeMISImportAttendance($query)
    {
        return $query->whereIn('command', ['mis:import-attendance', 'mis:import-attendance --update']);
    }
}
