<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Option extends Model
{
    use SoftDeletes;


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['json' => 'json'];


    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['option'];


    /**
     * Saves a new option or updates an existing one via type or id
     *
     * @param  $typeOrId    The type of option or the id of a option row
     * @param  $params      Array of params / option name / text / json
     * @return
     */
    public function saveOption($typeOrId, $params) 
    {
        if (is_int($typeOrId)) {
            $opt = Option::findOrFail($typeOrId);
        } else {
            $opt = Option::firstOrNew(['option' => $typeOrId]);
        }
        if (isset($params['text'])) {
            $opt->text = $params['text'];
        }
        if (isset($params['json'])) {
            $opt->json = $params['json'];
        }
        $opt->save();
    }


    /**
     * Loads all options or just specified ones
     * 
     * @param  $types           Array of options to load, if empty all options are loaded
     * @param  $includeDeleted  Boolean should we included deleted options
     * @return Collection or Option
     */
    public function loadOptions($types = array(), $includeDeleted = false)
    {
        if (count($types) > 0) {
            if ($includeDeleted) {
                $options = Option::withTrashed()->whereIn('option', $types)->get();
            } else {
                $options = Option::whereIn('option', $types)->get();
            }
        } else {
            if ($includeDeleted) {
                $options = Option::withTrashed()->get();
            } else {
                $options = Option::get();
            }
        }

        if (count($options) == 1) {
            return $options[0];
        }
        return $options;
    }


    /**
     * Loads a single option
     * 
     * @param  $type       
     * @param  $includeDeleted [description]
     * @return Collection
     */
    public function loadOption($type, $includeDeleted = false)
    {
        return Option::loadOptions([$type], $includeDeleted);
    }

}
