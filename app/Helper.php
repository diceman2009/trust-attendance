<?php

namespace App;

use Carbon\Carbon;

class Helper
{
    /**
     * Correctly capitalize a name (with support for double barrel names etc)
     * 
     * @param  $name
     * @return String
     */
    public static function capitalizeName($name)
    {
        $wordSplitters = [" ", "-", "O'", "L'", "D'", "St.", "Mc"];
        $lowercaseExceptions = ["l'", "d'", "van", "den", "von", "und", "der", "de", "da"];
        $name = ucwords(strtolower($name));
        foreach ($wordSplitters as $delimiter) {
            $words = explode($delimiter, $name);
            if (count($words) > 1) {
                $newwords = [];
                foreach ($words as $word) {
                    if (!in_array($word, $lowercaseExceptions)) {
                        $word = ucfirst($word);
                    }
                    array_push($newwords, $word);
                }
                if (in_array(strtolower($delimiter), $lowercaseExceptions)) {
                    $delimiter = strtolower($delimiter);
                }
                $name = join($delimiter, $newwords);
            }
        }
        return $name;
    }


    /**
     * Get a date for the start of this academic year
     * 
     * @return String (date in format yyyy-mm-dd)
     */
    public static function getAcademicYearStart() {
        $date = Carbon::createFromDate(null, 8, 15);
        if ($date->gt(Carbon::now())) {
            $date->subYear();
        }
        return $date->toDateString();
    }


    /**
     * Splits an array into groups using a prefix for for sample user_id and email_id would be split into two arrays
     * 
     * @return Array
     */
    public static function splitArrayByPrefix($array, $prefix1, $prefix2) {
        $prefixArray1 = [];
        $prefixArray2 = [];
        if (count($array) > 0) {
            foreach ($array as $key => $value) {
                $prefixSplit1 = explode($prefix1, $value, 2);
                if (count($prefixSplit1) == 2) {
                    array_push($prefixArray1, $prefixSplit1[1]);
                } else {
                    $prefixSplit2 = explode($prefix2, $value, 2);
                    if (count($prefixSplit2) == 2) {
                        array_push($prefixArray2, $prefixSplit2[1]);
                    }
                }
            }
        }
        return [
            $prefix1 => $prefixArray1,
            $prefix2 => $prefixArray2
        ];
    }
}
