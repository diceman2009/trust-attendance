<?php

namespace App;

use App\User;
use App\UserActivityLog;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserActivityLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_activity_log';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['event'];


    /**
     * Does this model use timestamps
     * 
     * @var boolean
     */
    public $timestamps = false;

    
    /**
     * Boot function for creating a new log
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        UserActivityLog::creating(function ($log)
        {
            $actual = User::actualUser();
            if ($actual) {
                $log->user_id = $actual->id;
                $log->username = $actual->username;
                $log->ip = request()->ip();
                $log->added_on = Carbon::now()->toDateTimeString();
            }
        });
    }
}
