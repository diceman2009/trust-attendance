<?php

namespace App;

use App\User;
use File;
use Mail;

class Email
{
    /**
     * Send an email
     * 
     * @param  string  $subject     The subject of the email
     * @param  string  $message     The message part of the email, can be html
     * @param  array   $sendTo      Array of arrays, like so ['userids' => [], 'groupids' => [], 'emails' => []]
     * @param  array   $replyTo     Reply to email address (optional) ['name' => '', 'email' => '']
     * @param  array   $attachments An array of attachments to add to the email like [['type' => 'text/csv', 'name' => 'myfile.csv', 'content' => 'base64encodedstring']]
     * @param  boolean $toParents   Are these emails being send to parents? if so we change the from name to the school title
     * @return boolean
     */
    public function send($subject = '', $message = '', $sendTo = [], $replyTo = null, $attachments = [], $toParents = false)
    {
        $sendToArray = [];
        if (isset($sendTo['userids']) && count($sendTo['userids']) > 0) {
            $users = User::whereIn('id', $sendTo['userids'])->get(['forename', 'surname', 'email']);
            foreach ($users as $user) {
                if ($this->validateUniqueEmail(array_column($sendToArray, 'email'), $user->email)) {
                    array_push($sendToArray, [
                        'name'  => $user->forename . ' ' . $user->surname,
                        'email' => $user->email
                    ]);
                }
            }
        }
        if (isset($sendTo['emails']) && count($sendTo['emails']) > 0) {
            foreach ($sendTo['emails'] as $email) {
                if ($this->validateUniqueEmail(array_column($sendToArray, 'email'), $email)) {
                    array_push($sendToArray, [
                        'name'  => null,
                        'email' => $email
                    ]);
                }
            }
        }
        if (count($sendToArray) == 0) {
            return false;
        }
        $sendToArray = array_slice($sendToArray, 0, config('mail.send_limit'));
        
        Mail::send('emails.default', [
                'subject' => $this->removeProfanity($subject), 
                'content' => $this->removeProfanity($message)
            ], function ($msg) use ($subject, $sendToArray, $replyTo, $attachments, $toParents) {

            $msg->subject($this->removeProfanity($subject));

            foreach ($attachments as $attachment) {
                $msg->attachData($attachment['content'], $attachment['name'], ['mime' => $attachment['type']]); 
            }

            if ($toParents) {
                $msg->from(config('mail.from.address'), config('app.school_title'));
            }

            if ($replyTo && !filter_var($replyTo['email'], FILTER_VALIDATE_EMAIL) === false) {
                $msg->replyTo($replyTo['email'], $replyTo['name']);
            }

            $recipientVars = [];
            foreach ($sendToArray as $val) {
                $msg->to($val['email'], $val['name']);
                $recipientVars[$val['email']] = ['id' => count($recipientVars)];
            }

            $to = $msg->getHeaders()->get('To');
            $to->setAddresses(array_column($sendToArray, 'email'));

            $headers = $msg->getHeaders();
            $headers->addTextHeader('X-Mailgun-Recipient-Variables', json_encode($recipientVars));
            $headers->addTextHeader('X-Mailgun-Tag', config('app.school_friendly_title'));
        });

        return true;
    }


    /**
     * Verifies the email isnt already in our sending array and that it is correctly formatted
     * 
     * @param  $array
     * @param  $email
     * @return Boolean
     */
    private function validateUniqueEmail($array, $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            if (array_search($email, $array) === false) {
                return true;
            }
        }
        return false;
    }


    /**
     * Profanity Regexp Patterns
     * @var Array
     */
    private $profanityPatterns = [
        '/\barse/i', '/\bass\b/i', '/bastard/i', '/bitch/i', '/clit/i', '/\bcock/i', '/\bcoon/i', '/cunt/i', '/dick/i', '/\bfag\b/i', '/faggot/i', '/fuck/i', 
        '/muff/i', '/nigga/i', '/nigger/i', '/piss/i', '/prick/i', '/pussy/i', '/rimjob/i', '/shit/i', '/slut/i', '/snatch/i', '/tosser/i', '/twat/i', 
        '/wank/i', '/whore/i', '/dildo/i', '/\bbollock/i'
    ];


    /**
     * Profanity Replacements
     * @var Array
     */
    private $profanityReplacements = [
        'a*se', 'a**', 'b**tard', 'b*tch', 'cl*t', 'c**k', 'c**n', 'c*nt', 'd*ck', 'f*g', 'f**got', 'f*ck', 'm*ff', 'n**ga', 'n**ger', 
        'p*ss', 'pr**k', 'p**sy', 'r*mjob', 'sh*t', 'sl*t', 's**tch', 't**ser', 't**t', 'w**k', 'w**re', 'd*ldo', 'b*llock'
    ];


    /**
     * Replaces profanity to pass through spam filters
     * 
     * @param $message
     * @return String
     */
    public function removeProfanity($message)
    {
        if ($message != '') {
            return preg_replace($this->profanityPatterns, $this->profanityReplacements, $message);
        }
        return '';
    }
}
