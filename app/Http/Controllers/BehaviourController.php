<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Output;
use App\Http\Requests;

class BehaviourController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display Behaviour Page
     *
     * @return Response
     */
    public function index()
    {
        return Output::page('behaviour');
    }
}
