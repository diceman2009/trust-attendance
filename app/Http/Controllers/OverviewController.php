<?php

namespace App\Http\Controllers;

use App\Http\Output;
use App\Http\Requests;
use App\School;
use DB;
use Illuminate\Http\Request;

class OverviewController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display overview
     *
     * @return Response
     */
    public function index()
    {
        $schools = School::where('attendance', 1)->ordered()->get();

        $attData = DB::table('attendance')
             ->select(DB::raw('count(*) as count, school_id, attendance_mark'))
             ->groupBy('school_id')
             ->groupBy('attendance_mark')
             ->get();

        $data = [];
        foreach ($schools as $school) {
            $add = (object) [
                'school_id' => $school->id,
                'name'      => $school->name,
                'present'   => 0,
                'auth'      => 0,
                'unauth'    => 0,
                'sum'       => 0
            ];
            foreach ($attData as $row) {
                if ($row->school_id == $school->id) {
                    if ($row->attendance_mark == 'PRESENT') {
                        $add->present = $row->count;
                    } else if ($row->attendance_mark == 'AUTHORISED') {
                        $add->auth = $row->count;
                    } else if ($row->attendance_mark == 'UNAUTHORISED') {
                        $add->unauth = $row->count;
                    }
                    if ($row->attendance_mark != 'VOID') {
                        $add->sum += $row->count;
                    }
                }
            }
            array_push($data, $add);
        }

        return Output::page('overview', [
            'attendance' => $data
        ]);
    }
}
