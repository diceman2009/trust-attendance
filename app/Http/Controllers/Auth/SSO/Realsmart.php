<?php

namespace App\Http\Controllers\Auth\SSO;

use App\Http\Output;
use App\User;
use App\UserActivityLog;
use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Request;
use Session;

class Realsmart
{
    /**
     * Contains the configuration details for the realsmart sso service
     * 
     * @var Array
     */
    private $config;


    public function __construct()
    {
        $this->config = [
            'enabled'           => config('app.sso_realsmart_enabled'),
            'url'               => config('app.sso_realsmart_url'),
            'deviceID'          => config('app.sso_realsmart_site_id'),
            'createAccounts'    => config('app.sso_realsmart_create_accounts')
        ];
    }


    /**
     * Attempt a single sign on for realsmart
     * 
     * @param  $forWidget (are we loading a single page widget?)
     * @return Response
     */
    public function attemptSignOn($forWidget)
    {

        //verify our app config variables
        if ($this->config['enabled'] == false || $this->config['url'] == '' || $this->config['deviceID'] == '') {
            return Output::page('errors.custom', [
                'error' => 'This SSO service is currently unconfigured.'
            ]);
        }

        //redirect away to sso url if no secret added
        if (!isset($_GET['token'])) {
            if ($forWidget) {
                return redirect($this->config['url'] . '/?third_party_token_sso=' . urlencode(url('login/sso/realsmart/1')));
            }
            return redirect($this->config['url']);
        }

        //perform data request to get user data
        $url = 'https://www.rlsmart.net/sso/cloud/identity.php?token=' . $_GET['token'];
        $guzzle = new Client();
        
        try {
            $response = $guzzle->request('GET', $url);
        } catch (BadResponseException $e) {
            return Output::page('errors.custom', [
                'error' => 'An error occured during the SSO attempt, please contact your administrator.'
            ]);
        }

        $result = $response->getBody();
        if ($result == '') {
            return Output::page('errors.custom', [
                'error' => 'An error occured during the SSO attempt, please contact your administrator.'
            ]);            
        }

        if ($result == "Error: Your login has timed out. Please go to your realsmartcloud site and log back in again before clicking this link." || $result == "Error: You are not logged in anymore. Please go back to the login form and log in again before clicking into this link.") {
            return Output::page('errors.custom', [
                'error' => 'Your realsmart login token has expired. Please logout of realsmart and try again.'
            ]);
        }

        $json = json_decode($result);
        $username = $json->username;
        $forename = $json->firstname;
        $surname = $json->surname;
        $email = $json->email;
        $staff = (strtolower($json->type) == "teacher" || strtolower($json->type) == "admin" || strtolower($json->type) == "administrator") ? true : false;
        $siteID = $json->site_id;

        if ($this->config['deviceID'] != $siteID || $username == '') {
            return Output::page('errors.custom', [
                'error' => 'This account doesn\'t have the required permissions to access the system.'
            ]);
        }

        //if we are using a widget we dont require the user to actually authenticate, just flash a username to the session
        if ($forWidget) {
            Session::flash('widget_username', $username);
            return redirect()->intended('/');
        }

        if (!$staff) {
            return Output::page('errors.custom', [
                'error' => 'This account doesn\'t have the required permissions to access the system.'
            ]);
        }

        //perform user creation if needed
        $user = User::where('username', $username)->first();
        if (empty($user)) {
            if (!$this->config['createAccounts']) {
                return Output::page('errors.custom', [
                    'error' => 'You don\'t have a matching account on the system, please contact your administrator.'
                ]);
            }

            //create new account
            $newUser = [
                'forename'  => $forename,
                'surname'   => $surname,
                'username'  => $username,
                'role'      => 'Staff',
                'email'     => $email,
                'password'  => '',
                'sso'       => true
            ];

            $user = new User;
            if ($user->createNew($newUser, true)) {
                return Output::page('errors.custom', [
                    'error' => 'Account creation validation failed, please contact your administrator.'
                ]);
            }
            $user = User::where('username', $username)->first();
        }

        //block disabled sso users
        if (!$user->allow_sso) {
            return Output::page('errors.custom', [
                'error' => 'This account is not allowed for single sign on.'
            ]);
        }

        //login
        Auth::loginUsingId($user->id);
        session(['sso' => 'realsmart']);

        //log activity
        UserActivityLog::create(['event' => 'realsmart sso login']);

        return redirect()->intended('/');
    }
}
