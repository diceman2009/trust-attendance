<?php

namespace App\Http\Controllers\Auth\SSO;

use App\Http\Output;
use App\User;
use App\UserActivityLog;
use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Request;
use Session;

class Firefly
{
    /**
     * Contains the configuration details for the firefly sso service
     * 
     * @var Array
     */
    private $config;


    public function __construct()
    {
        $this->config = [
            'enabled'           => config('app.sso_firefly_enabled'),
            'url'               => config('app.sso_firefly_url'),
            'deviceID'          => config('app.sso_firefly_device_id'),
            'createAccounts'    => config('app.sso_firefly_create_accounts')
        ];
    }


    /**
     * Attempt a single sign on for firefly
     * 
     * @param  $forWidget (are we loading a single page widget?)
     * @return Response
     */
    public function attemptSignOn($forWidget)
    {

        //verify our app config variables
        if ($this->config['enabled'] == false || $this->config['url'] == '' || $this->config['deviceID'] == '') {
            return Output::page('errors.custom', [
                'error' => 'This SSO service is currently unconfigured.'
            ]);
        }

        //redirect away to sso url if no secret added
        if (!isset($_GET['ffauth_secret'])) {
            return redirect(
                $this->config['url'] . '/login/api/webgettoken?app=' . $this->config['deviceID'] .
                '&failURL=' . urlencode(url('/login/sso/fail')) .
                '&successURL=' . urlencode(($forWidget) ? url('login/sso/firefly/1') : url('/login/sso/firefly'))
            );
        }

        //perform data request to get user data
        $url = $this->config['url'] . '/login/api/sso?ffauth_device_id=' . $this->config['deviceID'] . '&ffauth_secret=' . $_GET['ffauth_secret'];
        $guzzle = new Client();
        
        try {
            $response = $guzzle->request('GET', $url);
        } catch (BadResponseException $e) {
            return Output::page('errors.custom', [
                'error' => 'An error occured during the SSO attempt, please contact your administrator.'
            ]);
        }

        $result = $response->getBody();
        if ($result == '') {
            return Output::page('errors.custom', [
                'error' => 'An error occured during the SSO attempt, please contact your administrator.'
            ]);            
        }
        
        $xml = new \SimpleXMLElement($result);
        $username = (string)$xml->user['username'];
        $nameArr = explode(' ', (string)$xml->user['name'], 2);
        $forename = (count($nameArr) > 0) ? $nameArr[0] : '';
        $surname = (count($nameArr) > 1) ? $nameArr[1] : '';
        $email = (string)$xml->user['email'];
        $staff = ((string)$xml->user['canSetTask'] == 'yes') ? true : false;

        if ($username == '') {
            return Output::page('errors.custom', [
                'error' => 'This account doesn\'t have the required permissions to access the system.'
            ]);
        }

        //if we are using a widget we dont require the user to actually authenticate, just flash a username to the session
        if ($forWidget) {
            Session::flash('widget_username', $username);
            return redirect()->intended('/');
        }

        if (!$staff) {
            return Output::page('errors.custom', [
                'error' => 'This account doesn\'t have the required permissions to access the system.'
            ]);
        }

        //perform user creation if needed
        $user = User::where('username', $username)->first();
        if (empty($user)) {
            if (!$this->config['createAccounts']) {
                return Output::page('errors.custom', [
                    'error' => 'You don\'t have a matching account on the system, please contact your administrator.'
                ]);
            }

            //create new account
            $newUser = [
                'forename'  => $forename,
                'surname'   => $surname,
                'username'  => $username,
                'role'      => 'Staff',
                'email'     => $email,
                'password'  => '',
                'sso'       => true
            ];

            $user = new User;
            if ($user->createNew($newUser, true)) {
                return Output::page('errors.custom', [
                    'error' => 'Account creation validation failed, please contact your administrator.'
                ]);
            }
            $user = User::where('username', $username)->first();
        }

        //block disabled sso users
        if (!$user->allow_sso) {
            return Output::page('errors.custom', [
                'error' => 'This account is not allowed for single sign on.'
            ]);
        }

        //login
        Auth::loginUsingId($user->id);
        session(['sso' => 'firefly']);

        //log activity
        UserActivityLog::create(['event' => 'firefly sso login']);

        return redirect()->intended('/');
    }
}
