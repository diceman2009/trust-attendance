<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Output;
use App\Role;
use App\User;
use App\UserActivityLog;
use Auth;
use Gate;
use Illuminate\Http\Request;
use Session;

class ImpersonateController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display user impersonate modal
     * 
     * @return Response
     */
    public function index()
    {
        $actual = User::actualUser();

        $roles = [];
        $permissions = $actual->role->permissions()->get();
        $possibles = ['impersonate_admin', 'impersonate_privileged_staff', 'impersonate_staff'];
        foreach ($permissions as $permission) {
            if (array_search($permission->name, $possibles) !== false) {
                $role = Role::where('name', substr($permission->name, 12))->take(1)->get();
                if ($role) {
                    array_push($roles, $role[0]->id);
                }
            }
        }
        if (count($roles) == 0) {
            abort(403);
        }

        return Output::view('modals.impersonate', [
            'users' => User::whereIn('role_id', $roles)
                ->where('id', '!=', $actual->id)
                ->orderBy('forename')
                ->orderBy('surname')
                ->get(['id', 'forename', 'surname', 'username'])
        ]);
    }


    /**
     * Impersonate as another user (if allowed)
     * 
     * @return Response
     */
    public function impersonate(Request $request)
    { 
        $impersonate = User::findOrFail($request->user_id);
        $actual = User::actualUser();

        $permissions = $actual->role->permissions()->get()->lists('name');
        if ($permissions->contains('impersonate_' . $impersonate->role->name)) {
            UserActivityLog::create(['event' => 'impersonate ' . $impersonate->id]);

            $sso = Session::get('sso');
            if ($sso) {
                Session::put('impersonate_actual_sso', $sso);
            }
            Session::forget('sso');
            Session::put('impersonate_actual', $actual->id);

            Auth::loginUsingId($impersonate->id);
        } else {
            abort(403);
        }

        return Output::json();
    }


    /**
     * Resets us back to our initial login (if that exists)
     * 
     * @return Redirect
     */
    public function unimpersonate()
    {
        $id = Session::get('impersonate_actual');
        $sso = Session::get('impersonate_actual_sso');

        if ($id) {
            $user = User::findOrFail($id);
            if ($sso) {
                Session::put('sso', $sso);
            }
            Session::forget('impersonate_actual');
            Session::forget('impersonate_actual_sso');

            Auth::loginUsingId($user->id);
        }

        return redirect('/');
    }
}
