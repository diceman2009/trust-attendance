<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Output;
use App\UserActivityLog;
use Auth;
use Illuminate\Cache\RateLimiter;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Redirect;
use Validator;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Login with username not email
     * 
     * @var string
     */
    protected $username = 'username';


    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    

    /**
     * Executed after user has been authenticated successfully
     * 
     * @param  Request $request
     * @param  User $user
     * @return Json
     */
    public function authenticated($request, $user)
    {
        return Output::json([
            'intended' => Redirect::intended('/')->getTargetUrl()
        ]);
    }

    /**
     * Handle a login request to the application (replaces the default to add ajax support and removes remember ability)
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            $seconds = app(RateLimiter::class)->availableIn(
                $this->getThrottleKey($request)
            );
            return Output::jsonError($seconds);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::guard($this->getGuard())->attempt($credentials)) {

            //log activity
            UserActivityLog::create(['event' => 'login']);

            //clean session
            session(['sso' => null, 'impersonate_actual' => null]);

            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return Output::jsonError($this->getFailedLoginMessage());
    }


    /**
     * Log the user out of the application.
     *
     * @return Response
     */
    public function logout()
    {
        //log activity
        UserActivityLog::create(['event' => 'logout']);

        //clean session
        session(['sso' => null, 'impersonate_actual' => null]);

        Auth::guard($this->getGuard())->logout();

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }
}
