<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Auth\SSO\Firefly;
use App\Http\Controllers\Auth\SSO\Realsmart;
use App\Http\Controllers\Controller;
use App\Http\Output;
use App\Http\Requests;
use Auth;
use Illuminate\Http\Request;
use Session;

class SingleSignOnController extends Controller
{

    /**
     * Begin singlesignon for a given service
     * 
     * @param  $service
     * @param  $forWidget (are we loading a single page widget?)
     * @return Response or Redirect
     */
    public function index($service, $forWidget = false)
    {   
        if (!$forWidget) {
            Auth::logout();
        }

        $service = strtolower($service);
        if ($service === 'firefly') {
            $sso = new Firefly();
        } elseif ($service === 'realsmart') {
            $sso = new Realsmart();
        }

        if (!isset($sso)) {
            return Output::page('errors.custom', [
                'error' => 'This SSO service does not exist.'
            ]);
        }

        Session::forget('impersonate_actual');
        return $sso->attemptSignOn($forWidget);
    }


    /**
     * Outputs a error page for generic SSO failure
     * 
     * @param  Request $request
     * @return Response
     */
    public function fail(Request $request)
    {
        Auth::logout();

        return Output::page('errors.custom', [
            'error' => 'The single sign on attempt failed, please contact your administrator.'
        ]);
    }
}
