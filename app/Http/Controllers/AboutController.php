<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Output;
use Auth;

class AboutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display about modal
     * 
     * @return Response
     */
    public function index()
    {   

        $client = new \Wonde\Client(config('app.wonde_api_token'));
        $school = $client->school('A418876919');



        dd($client->attendanceCodes->all());
        // foreach ($school->attendance->all([], $params) as $rec) {
        //     dd($rec);
        // }



        return Output::view('modals.about', [
            'user' => Auth::user(),
            'misImport' => 'a'//ScheduleLog::misImport()->complete()->first()->formatted_created_at
        ]);
    }
}
