<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Output;
use App\Http\Requests;
use App\Role;
use App\User;
use Auth;
use Carbon\Carbon;
use Gate;
use Hash;
use Illuminate\Http\Request;
use League\Csv\Reader;
use League\Csv\Writer;
use Validator;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display user list modal
     * 
     * @return Response
     */
    public function index()
    {
        if (Gate::denies('manage_users')) {
            abort(403);
        }

        return Output::view('modals.users', [
            'users' => User::with('role')
               ->where('role_id', '!=', 1)
               ->ordered()
               ->get(['id', 'forename', 'surname', 'username', 'role_id', 'email', 'allow_sso'])
        ]);
    }


    /**
     * Display make new user modal
     * 
     * @return Response
     */
    public function create()
    {
        if (Gate::denies('manage_users')) {
            abort(403);
        }

        return Output::view('modals.users-add');
    }


    /**
     * Register new user
     * 
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manage_users')) {
            abort(403);
        }

        $user = new User;
        $errors = $user->createNew($request->all());

        if ($errors) {
            return Output::jsonValidationError($errors);
        }

        return Output::json();
    }


    /**
     * Display edit user modal
     * 
     * @param  $id
     * @return Response
     */
    public function edit($id)
    {
        if (Gate::denies('manage_users')) {
            abort(403);
        }

        $user = User::findOrFail($id);
        if ($user->role_id == 1) {
            abort(403);
        }

        return Output::view('modals.users-add', ['user' => $user]);
    }


    /**
     * Update an existing user (password is optional)
     * 
     * @param  Request $request
     * @param  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('manage_users')) {
            abort(403);
        }

        $user = User::findOrFail($id);

        $errors = $user->updateExisting($request->all());
        if ($errors) {
            return Output::jsonValidationError($errors);
        }

        return Output::json();
    }


    /**
     * Display change password modal
     * 
     * @return Response
     */
    public function showChangePassword()
    {
        if (session('sso')) {
            abort(403);
        }

        return Output::view('modals.changepassword');
    }


    /**
     * Change logged in users password
     * 
     * @param  Request $request
     * @return Response
     */
    public function changePassword(Request $request)
    {
        if (session('sso')) {
            abort(403);
        }
        
        $user = $request->user();

        $validator = Validator::make($request->all(), [
            'current_password'  => 'required',
            'new_password'  => 'required|confirmed|different:current_password|min:8|max:125|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
        ]);

        $validator->after(function($validator) use ($request, $user) {
            if (!Hash::check($request->current_password, $user->password)) {
                $validator->errors()->add('current_password', 'The current password is incorrect.');
            }
        });

        if ($validator->fails()) {
            return Output::jsonValidationError($validator->messages());
        }

        $user->password = $request->new_password;
        $user->save();

        return Output::json();
    }


    /**
     * Delete a user
     * 
     * @param  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manage_users')) {
            abort(403);
        }

        $user = User::find($id);
        if ($user) {
            if ($user->role_id == 1) {
                abort(403);
            }
            $user->delete();
        }

        return Output::json();
    }


    /**
     * Display user import/export modal
     * 
     * @return Response
     */
    public function importModal()
    {
        if (Gate::denies('manage_users')) {
            abort(403);
        }

        return Output::view('modals.users-import');
    }
    

    /**
     * Takes csv upload and converts it to JSON while validating a few things
     * 
     * @return Response
     */
    public function import(Request $request)
    {
        if (Gate::denies('manage_users')) {
            abort(403);
        }

        if (!ini_get('auto_detect_line_endings')) { //league csv wants this to help with osx support
            ini_set('auto_detect_line_endings', '1');
        }

        Output::$useJsonHeaders = false; //fix for ie issues

        $validator = Validator::make($request->all(), [
            'Filedata'       => 'required|max:2000|mimes:csv,txt',
        ], [
            'Filedata.max'   => 'The file needs to be less than 2MB in size.',
            'Filedata.mimes' => 'File type not allowed, it must be a .csv file.'
        ]);

        if ($validator->fails()) {
            return Output::jsonValidationError($validator->messages(), false);
        }

        $csv = Reader::createFromPath($_FILES['Filedata']['tmp_name']);
        $userImport = $csv->fetchAll();
        if (count($userImport) < 2) {
            return Output::jsonError('', ['Filedata' => 'The CSV file must contain at least 1 row with user data.']);
        }

        $firstRow = $csv->fetchOne();
        $columns = [
            ['title' => 'UID',           'key' => 'uid',            'required' => false],
            ['title' => 'Username',      'key' => 'username',       'required' => true],
            ['title' => 'Forename',      'key' => 'forename',       'required' => true],
            ['title' => 'Surname',       'key' => 'surname',        'required' => true],
            ['title' => 'Role',          'key' => 'role',           'required' => true],
            ['title' => 'Email Address', 'key' => 'email_address',  'required' => true],
            ['title' => 'SSO Allowed',   'key' => 'sso_allowed',    'required' => false],
            ['title' => 'Password',      'key' => 'password',       'required' => false]
        ];
        if (config('app.sso_firefly_enabled') || config('app.sso_realsmart_enabled')) {
            $columns[7]['required'] = true;
        }

        $mappings = [];
        foreach ($columns as $index => $column) {
            $loc = array_search($column['title'], $firstRow);
            if ($loc !== false) {
                $mappings[$index] = $loc;
            } elseif ($column['required']) {
                return Output::jsonError('', ['Filedata' => 'The CSV file must contain a column with the heading \'' . $column['title'] . '\'.']);
            }
        }

        $formattedImport = [];
        foreach ($userImport as $key => $row) {
            $obj = (object) [];
            if ($key > 0) {
                foreach ($mappings as $index => $location) {
                    $obj->{$columns[$index]['key']} = (isset($row[$location])) ? $row[$location] : '';
                }
                array_push($formattedImport, $obj);
            }
        }

        return Output::json($formattedImport);
    }


    /**
     * Creates CSV of all users and triggers a download
     * 
     * @return Response CSV
     */
    public function export()
    {
        if (Gate::denies('manage_users')) {
            abort(403);
        }

        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        $headings = ['UID', 'Username', 'Forename', 'Surname', 'Role', 'Email Address', 'Password'];
        if (config('app.sso_firefly_enabled') || config('app.sso_realsmart_enabled')) {
            array_splice($headings, 6, 0, 'SSO Allowed');
        }
        $csv->insertOne($headings);

        $users = User::with('role')->get();
        foreach ($users as $user) {
            if ($user->role_id != 1) { //we hide superadmins
                $arr = [
                    $user->id,
                    $user->username,
                    $user->forename,
                    $user->surname,
                    $user->role->label,
                    $user->email
                ];
                if (config('app.sso_firefly_enabled') || config('app.sso_realsmart_enabled')) {
                    array_push($arr, $user->allow_sso);
                }
                $csv->insertOne($arr);
            }            
        }

        $csv->output('User Export (' . Carbon::now()->toFormattedDateString() . ').csv');
        die();
    }
}
