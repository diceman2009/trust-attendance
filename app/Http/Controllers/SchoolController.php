<?php

namespace App\Http\Controllers;

use App\Http\Output;
use App\Http\Requests;
use App\School;
use App\Student;
use App\Attendance;
use App\AttendanceCode;
use Carbon\Carbon;

class SchoolController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display school page
     *
     * @return Response
     */
    public function index($schoolId, $dateId = 4, $startDate = null, $endDate = null)
    {

        $range = $this->getDateRange($dateId, $startDate, $endDate);

        $school = School::findOrFail($schoolId);

        $years = [7];
        $students = Student::with('attendance')
            ->whereIn('year', $years)
            ->where('school', '=', $school->wonde_id)
            ->get();
        
        $attendanceCodes = AttendanceCode::where('type', '!=', 'VOID')
            ->pluck('type')
            ->toArray();       
        $attendanceCodes = array_unique($attendanceCodes);

        $empty = (object)[];
        $totals = (object)[];
        foreach($attendanceCodes as $code){
            $empty->$code = 0;
            $totals->$code = 0;
        }

        foreach($students as $student){
            $student->summary = clone $empty;
            foreach($student->attendance as $attendance){
                $type = $attendance->code->type;
                if($type != "VOID"){       
                    $student->summary->$type++;
                    $totals->$type++;
                }
            }
        }

        //CONVERT TO PERCENTAGE
        foreach($students as $student){
            $total = 0;
            foreach($student->summary as $summary){
                $total+=$summary;
            }
            if($total > 0){
                $student->summary->PRESENT = round($student->summary->PRESENT/$total * 100, 1);
                $student->summary->AUTHORISED = round($student->summary->AUTHORISED/$total * 100, 1);
                $student->summary->UNAUTHORISED = round($student->summary->UNAUTHORISED/$total * 100, 1);
            }else{
                $student->display = 'no';
            }
        }
        return Output::page('school', [
            'school' => $school,
            'students' => $students,
            'attendanceCodes' => $attendanceCodes,
            'dateId' => 4,
            'reportId' => 0,
            'totals' => $totals
        ]);
    }

        /**
     * Make the start and end dates for our report (returns an object with start and end properties)
     * 
     * @param  $dateId
     * @param  $startDate (only with the custom dateid) yyyy-mm-dd
     * @param  $endDate (only with the custom dateid) yyyy-mm-dd
     * @return Object
     */
    private function getDateRange($dateId, $startDate, $endDate)
    {
        $start = null;
        $end = null;

        if ($dateId == 0) {
            //this academic year
            $start = Helper::getAcademicYearStart();
        } elseif ($dateId == 1) {
            //between custom dates
            try {
                $startDate = Carbon::createFromFormat('Y-m-d', $startDate);
            } catch (\Exception $e) {
                abort(404);
            }
            try {
                $endDate = Carbon::createFromFormat('Y-m-d', $endDate);
            } catch (\Exception $e) {
                abort(404);
            }
            if ($endDate->isFuture()) {
                abort(404);
            }
            $start = $startDate->toDateString();
            $end = $endDate->toDateString();
        } elseif ($dateId == 2) {
            //today
            $start = Carbon::now()->toDateString();
        } elseif ($dateId == 3) {
            //yesterday
            $start = Carbon::yesterday()->toDateString();
        } elseif ($dateId == 4) {
            //past week
            $start = Carbon::now()->subWeek()->toDateString();
        } elseif ($dateId == 5) {
            //past month
            $start = Carbon::now()->subMonth()->toDateString();
        } elseif ($dateId == 6) {
            //past 3 months
            $start = Carbon::now()->subMonths(3)->toDateString();
        } elseif ($dateId == 7) {
            //past 6 months
            $start = Carbon::now()->subMonths(6)->toDateString();
        } elseif ($dateId == 8) {
            //past year
            $start = Carbon::now()->subYear()->toDateString();
        } elseif ($dateId == 9) {
            //past 2 years
            $start = Carbon::now()->subYears(2)->toDateString();
        }

        if (!isset($end)) {
            $end = Carbon::now()->toDateString();
        }

        if ($dateId != 1 && (isset($startDate) || isset($endDate))) {
            abort(404);
        }

        return (object) ['start' => $start, 'end' => $end];
    }
}
