<?php

// Route patterns
Route::pattern('id', '\d+');
Route::pattern('date', '^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$');


Route::group(['middleware' => ['web']], function () {

    // Main Pages
    Route::get('/', function () {
        return redirect('/attendance');
    });
    Route::get('home', function () {
        return redirect('/attendance');
    });
    //Route::get('overview', 'OverviewController@index');
    Route::get('attendance', 'OverviewController@index');
    Route::get('behaviour', 'BehaviourController@index');
    Route::get('school/{id}', 'SchoolController@index');

    // Authentication
    Route::get('login', 'Auth\AuthController@showLoginForm');
    Route::post('login', 'Auth\AuthController@login');
    Route::get('logout', 'Auth\AuthController@logout');
    Route::get('login/sso/fail', 'Auth\SingleSignOnController@fail');
    Route::get('login/sso/{service}/{forwidget?}', 'Auth\SingleSignOnController@index')->where('forwidget', '1');
    Route::get('unimpersonate', 'Auth\ImpersonateController@unimpersonate');

    // All Modal Related Routes
    Route::group(['prefix' => 'modal'], function () {
        
        Route::get('about', 'AboutController@index');

        // Change password
        Route::get('changepassword', 'UserController@showChangePassword');
        Route::post('changepassword', 'UserController@changePassword');

        // Impersonate
        Route::get('impersonate', 'Auth\ImpersonateController@index');
        Route::post('impersonate', 'Auth\ImpersonateController@impersonate');

        // User Management
        Route::resource('users', 'UserController', ['except' => ['show']]);
        Route::get('users/import', 'UserController@importModal');
        Route::post('users/import', 'UserController@import');
        Route::get('users/export', 'UserController@export');       
        
    });

});


// PHP Info Page
Route::get('info', function () {
    if (config('app.debug')) {
        phpinfo();
    }
});
