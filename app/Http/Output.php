<?php

namespace App\Http;

use Illuminate\Support\MessageBag;
use Response;

class Output
{
    /**
     * If set to false the webpage will not set the application/json headers
     * @var boolean
     */
    public static $useJsonHeaders = true;


    /**
     * Provide a json response with our expected format
     *
     * @param  Data payload  $data
     * @param  Response status (generally will be success or error) $status
     * @param  Some text to specify what happened during the request (optional)  $message
     */
    public static function json($data = null, $status = 'success', $message = '', $code = 200)
    {
        if ($status == 'error' && $code == 200) {
            $code = 500;
        }

        $outputArray = [
            'status'    => $status,
            'message'   => $message,
            'data'      => $data
        ];

        if (!self::$useJsonHeaders) {
            http_response_code($code);
            return json_encode($outputArray);
        }

        return Response::json($outputArray, $code);
    }


    /**
     * Provide error in json
     * 
     * @param  $message
     * @param  $code
     * @return Json Array
     */
    public static function jsonError($message = '', $errorArray = [], $code = 500)
    {
        return self::json(
            ['errors' => $errorArray],
            'error',
            $message,
            $code
        );
    }


    /**
     * Provide validation error in json
     * 
     * @param  $validator
     * @return Json Array
     */
    public static function jsonValidationError($errorArray, $includeHTML = true)
    {
        if (!($errorArray instanceof MessageBag)) {
            $errorArray = new MessageBag($errorArray);
        }

        $payload = ['errors' => $errorArray];
        if ($includeHTML) {
            $payload['html'] = trim(preg_replace('/\t+/', '', view('common.errors', ['errors' => $errorArray])->render()));
        }

        return self::json(
            $payload,
            'error',
            'Validation error',
            422
        );
    }


    /**
     * Make a view with optional prerendered
     *
     * @param  Name of the view  $view
     * @param  Data to be added to the view  $data
     * @param  Should the view be returned as html
     */
    private static function makeView($view, $data = [], $preRendered = false)
    {
        if ($preRendered) {
            return trim(preg_replace('/\t+/', '', view($view, $data)->render())); //strips tabs out too for a smaller data size
        }
        return view($view, $data);
    }


    /**
     * Provide a view response
     * 
     * @param  [type] $view [description]
     * @param  array  $data [description]
     * @return View
     */
    public static function view($view, $data = [])
    {
        if (request()->ajax()) {
            return self::makeView($view, $data, true);
        }
        
        return self::makeView($view, $data);
    }


    /**
     * Provide a page response
     *
     * @param  The initial request object  $request
     * @param  Name of the view  $view
     * @param  Data to be added to the view  $data
     * @return View
     */
    public static function page($view, $data = [])
    {
        if (request()->ajax()) {
            $data['onlyContent'] = true;
            return self::makeView($view, $data, true);
        }

        return self::makeView($view, $data);
    }


    /**
     * Provide a json response with a rendered view
     * @param  $view
     * @param  Array $data
     * @return Json Array
     */
    public static function jsonView($view, $data = [])
    {
        return self::json(
            self::makeView($view, $data, true)
        );
    }
}
