<?php

namespace App\Providers;

use App\Role;
use App\School;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('modals.users-add', function($view) {
            $view->with('roles', Role::where('name', '!=', 'super_admin')->get());
        });

        view()->composer('layouts.partials.navbar', function($view) {
            $view->with('primarySchools', School::primary()->ordered()->get(['id', 'name']));
            $view->with('secondarySchools', School::secondary()->ordered()->get(['id', 'name']));
        });

        view()->composer('overview', function($view) {
             $view->with('schools', School::get(['id', 'name']));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
