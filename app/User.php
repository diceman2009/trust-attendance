<?php

namespace App;

use App\Helper;
use App\User;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Session;
use Validator;

class User extends Authenticatable
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['forename', 'surname', 'username', 'email', 'password', 'allow_sso'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Employee relationship
     * 
     * @return Employee
     */
    public function employee()
    {
        return $this->hasOne('App\Employee', 'mis_id', 'miscode');
    }

    /**
     * Join forname and surname to make a fullname attribute
     * 
     * @return String
     */
    public function getFullNameAttribute()
    {
        return $this->attributes['forename'] . ' ' . $this->attributes['surname'];
    }


    /**
     * Join forname and surname to make a fullname attribute
     * 
     * @return String
     */
    public function getFormalFullNameAttribute()
    {
        return substr($this->attributes['forename'], 0, 1) . '. ' . $this->attributes['surname'];
    }

    
    /**
     * Encrypt the password
     * 
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }


    /**
     * Capitalize the forename
     * 
     * @param $value
     */
    public function setForenameAttribute($value)
    {
        $this->attributes['forename'] = Helper::capitalizeName($value);
    }


    /**
     * Capitalize the surname
     * 
     * @param $value
     */
    public function setSurnameAttribute($value)
    {
        $this->attributes['surname'] = Helper::capitalizeName($value);
    }


    /**
     * Lowercase the username
     * 
     * @param $value
     */
    public function setUsernameAttribute($value)
    {
        $this->attributes['username'] = strtolower($value);
    }


    /**
     * Lowercase the email
     * 
     * @param $value
     */
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }


    /**
     * Default sort order queryscope
     * 
     * @param  $query
     * @return Builder
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('surname')->orderBy('forename');
    }


    /**
     * Boot method used for binding onto the user delete event
     * 
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        //we require this because the eloquent cascade doesn't execute for softdeletes
        User::deleting(function ($user) {
           Reservation::where('user_id', '=', $user->id)->delete();
        });
    }

    /**
     * Role relationship
     * 
     * @return Role
     */
    public function role()
    {
        return $this->belongsTo('App\Role');
    }


    /**
     * Does the user have this role
     * 
     * @param $role
     * @return boolean
     */
    public function hasRole($role)
    {
        if (is_string($role)) {
            if ($this->role->name == $role) {
                return true;
            }
            return false;
        }

        foreach ($role as $r) {
            if ($this->hasRole($r->name)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Returns the actual user (regardless of impersonation)
     * 
     * @return User
     */
    public static function actualUser()
    {
        if (Session::get('impersonate_actual')) {
            $actual = User::findOrFail(Session::get('impersonate_actual'));
        } else {
            $actual = Auth::user();
        }
        
        return $actual;
    }


    /**
     * Create/register a new user (with validation)
     * 
     * @param  $userArray Array with user params
     * @return Array
     */
    public function createNew($userData, $fromSSO = false)
    {
        $validator = Validator::make($userData, [
            'username'  => 'required|regex:/^[^ ]+$/|max:125|unique:users,username,NULL,id,deleted_at,NULL',
            'password'  => 'confirmed|min:8|max:125|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
            'forename'  => 'required|max:125',
            'surname'   => 'required|max:125',
            'role'      => 'required|exists:roles,label|not_in:Super Admin',
            'email'     => 'required|email|max:200|unique:users,email,NULL,id,deleted_at,NULL',
            'sso'       => 'boolean'
        ]);

        if ($validator->fails()) {
            return $validator->messages();
        }

        $user = new User;
        $user->forename = $userData['forename'];
        $user->surname = $userData['surname'];
        $user->username = $userData['username'];
        $user->role_id = Role::where('label', $userData['role'])->first()->id;
        $user->email = $userData['email'];
        $user->password = $userData['password'];
        $user->created_via_sso = $fromSSO;
        $user->allow_sso = isset($userData['sso']);
        $user->save();

        return null;
    }


    /**
     * Update an existing user (with validation)
     * 
     * @param  $userArray Array with user params
     * @return Array
     */
    public function updateExisting($userData)
    {
        $validator = Validator::make($userData, [
            'username'  => 'required|regex:/^[^ ]+$/|max:125|unique:users,username,' . $this->id . ',id,deleted_at,NULL',
            'password'  => 'confirmed|min:8|max:125|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
            'forename'  => 'required|max:125',
            'surname'   => 'required|max:125',
            'role'      => 'required|exists:roles,label|not_in:Super Admin',
            'email'     => 'required|email|max:200|unique:users,email,' . $this->id . ',id,deleted_at,NULL',
            'sso'       => 'boolean'
        ]);

        if ($this->role_id == 1) { //we dont allow updating superadmins
            abort(403);
        }

        if ($validator->fails()) {
            return $validator->messages();
        }

        $this->forename = $userData['forename'];
        $this->surname = $userData['surname'];
        $this->username = $userData['username'];
        $this->role_id = Role::where('label', $userData['role'])->first()->id;
        $this->email = $userData['email'];
        $this->allow_sso = isset($userData['sso']);
        if ($userData['password'] != '') {
            $this->password = $userData['password'];
        }
        $this->save();

        return null;
    }
}
