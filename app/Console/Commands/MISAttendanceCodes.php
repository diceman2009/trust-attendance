<?php

namespace App\Console\Commands;

use App\Console\MISBase;
use App\AttendanceCode;
use App\ScheduleLog;

class MISAttendanceCodes extends MISBase
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mis:import-attendance-codes {--update : Will only update changes instead of doing a full import} {--force : Ignore our 50% size check and force an import}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import attendacne codes from the Wonde API';

    /**
     * Our existing objects
     *
     * @var array
     */
    protected $existing = null;

    /**
     * Holds counts of how many items are created/updated/deleted
     *
     * @var object
     */
    protected $counts = null;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $endPoint = (object)['singular'=>'attendance code','plural'=>'attendance codes','command'=>'mis:import-attendacne-codes'];

        $this->comment('Importing '.$endPoint->plural.' from the Wonde API');

        $log = ScheduleLog::create(['command' => $endPoint->command . (($this->option('update')) ? ' --update' : '') . (($this->option('force')) ? ' --force' : '')]);

        $client = new \Wonde\Client(config('app.wonde_api_token'));

        $scheduleLog = ScheduleLog::misImportAttendanceCodes();
        $last = $scheduleLog->complete()->first();
        $params = ($this->option('update') && $last) ? ['updated_after' => $last->updated_at] : [];

        $this->comment('Loading '.$endPoint->plural);

        $data = [];
        foreach ($client->attendanceCodes->all([''], $params) as $rec) {
            $obj = (object)[
                'data' => [
                    'wonde_id' => $rec->id,
                    'code' => $rec->code,
                    'description' => $rec->description,
                    'type' => $rec->type
                ]
            ];
            array_push($data, $obj);
        }

        MISBase::handlePossibilityForce($scheduleLog, $endPoint, $log, '', $this->option('force'), $client);

        $this->existing = AttendanceCode::get();
        $this->counts = (object) ['created' => 0, 'updated' => 0, 'deleted' => 0];

        MISBase::processData($data, $this, $last, $client, $endPoint, 'wonde_id');
        
        $summary = $this->counts->created . ' created, ' . $this->counts->updated . ' updated and ' . $this->counts->deleted . ' deleted';
        $this->line($summary);
        $this->comment($endPoint->plural. ' import complete');
        $log->update(['comment' => $summary]);
    }

    /**
     * Create a new record
     *
     * @param  $record
     * @return void
     */
    protected function create($record)
    {
        $this->info('Creating: ' . $record->data['wonde_id']);
        AttendanceCode::create($record->data);
        $this->counts->created++;
    }

    /**
     * Update an existing record
     *
     * @param  $record
     * @param  $loc (the position of this object in the $this->existing array)
     * @return void
     */
    protected function update($record, $loc)
    {
        $this->info('Updating: ' . $record->data['wonde_id']);
        $this->existing[$loc]->update($record->data);
        $this->existing->splice($loc, 1);
        $this->counts->updated++;
    }

    /**
     * Try to find, and if found, delete record
     *
     * @param  $id
     * @return void
     */
    protected function ifExistsDelete($id)
    {
        $object = AttendanceCode::where('wonde_id', $id)->first();
        if ($object) {
            $this->delete($object);
        }
    }
    /**
     * Delete an existing record
     *
     * @param  $record
     * @return void
     */
    protected function delete($record)
    {
        $this->info('Deleting: ' . $record->wonde_id);
        $record->delete();
        $this->counts->deleted++;
    }
}
