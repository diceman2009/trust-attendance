<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ScheduleLog;

class MISImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mis:import {--update : Will only update changes instead of foing a full import}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import MIS data from the Wonde API';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {               
        $this->comment('Performing MIS import');

        $log = ScheduleLog::create(['command' => 'mis:import' . (($this->option('update')) ? ' --update' : '')]);

        $params = ['--update' => $this->option('update')];
        $this->call('mis:import-attendance-codes', $params);
        $this->call('mis:import-attendances', $params);
        $this->call('mis:import-attendance', $params);
        $this->call('mis:import-students', $params);
        $this->comment('MIS import complete');

        $log->update(['comment' => 'complete']);
    }
}
