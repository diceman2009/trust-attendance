<?php

namespace App\Console\Commands;

use App\Console\MISBase;
use App\Employee;
use App\ScheduleLog;
use App\School;
use DB;

class MISAttendance extends MISBase
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mis:import-attendance {--update : Will only update changes instead of foing a full import}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import attendance from the Wonde API';

    /**
     * Our existing subjects
     *
     * @var array
     */
    protected $existing = null;

    /**
     * Holds counts of how many items are created/updated/deleted
     *
     * @var object
     */
    protected $counts = null;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Importing attendance from the Wonde API');

        $client = new \Wonde\Client('cbbd7916edbfd38773bac551fcb55c02dce23d88');
        
        $this->comment('Loading attendance codes'); 
        $attendanceCodes = [];
        foreach ($client->attendanceCodes->all() as $code) {
            $attendanceCodes[$code->id] = $code->type;
        }
        
        $schools = School::where('wonde_id', '!=', '')
            ->where('attendance', 1)
            ->get(['id', 'name', 'wonde_id']);

        foreach ($schools as $school) {
            $this->comment('Loading attendance for ' . $school->name);
            $wondeSchool = $client->school($school->wonde_id);
            foreach ($wondeSchool->attendance->all([], ['attendance_date_after' => '2016-09-01', 'attendance_date_before' => '2016-12-13']) as $attendance) {
                DB::table('attendance')
                    ->insert([
                        'school_id'       => $school->id,
                        'student_id'      => $attendance->student,
                        'session'         => $attendance->session,
                        'date'            => $attendance->date->date,
                        'attendance_mark' => $attendanceCodes[$attendance->attendance_code]
                    ]);
            }
        }

        $this->comment('Import complete');
    }
}
