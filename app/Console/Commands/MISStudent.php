<?php

namespace App\Console\Commands;

use App\Console\MISBase;
use App\Student;
use App\ScheduleLog;
use App\Helper;

class MISStudent extends MISBase
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mis:import-students {--update : Will only update changes instead of doing a full import} {--force : Ignore our 50% size check and force an import}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import students from the Wonde API';

    /**
     * Our existing objects
     *
     * @var array
     */
    protected $existing = null;

    /**
     * Holds counts of how many items are created/updated/deleted
     *
     * @var object
     */
    protected $counts = null;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $endPoint = (object)['singular'=>'student','plural'=>'students','command'=>'mis:import-students'];

        $this->comment('Importing '.$endPoint->plural.' from the Wonde API');

        $log = ScheduleLog::create(['command' => $endPoint->command . (($this->option('update')) ? ' --update' : '') . (($this->option('force')) ? ' --force' : '')]);

        $schoolWondeId = config('app.wonde_school_id');
        $client = new \Wonde\Client(config('app.wonde_api_token'));
        $school = $client->school($schoolWondeId);

        $scheduleLog = ScheduleLog::misImportStudents();
        $last = $scheduleLog->complete()->first();
		$params = ($this->option('update') && $last) ? ['updated_after' => $last->updated_at] : [];

        $this->comment('Loading '.$endPoint->plural);

        $data = [];
        foreach ($school->students->all(['extended_details', 'education_details', 'registration', 'year'], $params) as $student) {
        	$split = explode('|', $student->mis_id);
            $upn = (isset($student->education_details) && isset($student->education_details->data->upn)) ? $student->education_details->data->upn : '';
            $obj = (object)[
                'data' => [
                    'wonde_id' => $student->id,
                    'username' => $split[count($split) - 1],
                    'upn' => $upn,
                    'year' => (isset($student->year)) ? $student->year->data->name : '',
                    'tutor' => (isset($student->registration)) ? $student->registration->data->name : '',
                    'sex' => ($student->gender == 'FEMALE') ? 'F' : 'M',
                    'forename' => ($student->forename) ? Helper::capitalizeName($student->forename) : '',
                    'surname' => ($student->surname) ? Helper::capitalizeName($student->surname) : '',
                    'ethnicity' => (is_null($student->extended_details->data->ethnicity)) ? '' : $student->extended_details->data->ethnicity,
                    'school' => $schoolWondeId
                ]
            ];
            array_push($data, $obj);
        }

        MISBase::handlePossibilityForce($scheduleLog, $endPoint, $log, $school, $this->option('force'));

        $this->existing = Student::get();
        $this->counts = (object) ['created' => 0, 'updated' => 0, 'deleted' => 0];

        MISBase::processData($data, $this, $last, $school, $endPoint, 'wonde_id');
        
        $summary = $this->counts->created . ' created, ' . $this->counts->updated . ' updated and ' . $this->counts->deleted . ' deleted';
        $this->line($summary);
        $this->comment($endPoint->plural. ' import complete');
        $log->update(['comment' => $summary]);
    }

    /**
     * Create a new record
     *
     * @param  $record
     * @return void
     */
    protected function create($record)
    {
        $this->info('Creating: ' . $record->data['wonde_id']);
        Student::create($record->data);
        $this->counts->created++;
    }

    /**
     * Update an existing record
     *
     * @param  $record
     * @param  $loc (the position of this object in the $this->existing array)
     * @return void
     */
    protected function update($record, $loc)
    {
        $this->info('Updating: ' . $record->data['wonde_id']);
        $this->existing[$loc]->update($record->data);
        $this->existing->splice($loc, 1);
        $this->counts->updated++;
    }

    /**
     * Try to find, and if found, delete record
     *
     * @param  $id
     * @return void
     */
    protected function ifExistsDelete($id)
    {
        $object = Student::where('wonde_id', $id)->first();
        if ($object) {
            $this->delete($object);
        }
    }
    /**
     * Delete an existing record
     *
     * @param  $record
     * @return void
     */
    protected function delete($record)
    {
        $this->info('Deleting: ' . $record->wonde_id);
        $record->delete();
        $this->counts->deleted++;
    }
}
