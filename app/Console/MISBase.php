<?php

namespace App\Console;

use Illuminate\Console\Command;
use App\Option;
use App\Email;

class MISBase extends Command
{
    /**
     * Handle possibility of forcing an import, if we force, ignore the 50% safety check
     *  
     * @param  object $scheduleLog  Referenceing the log
     * @param  object $endPoint    	The data that we want to get from Wonde
     * @param  object $log    		Log that saves to ScheduleLog
     * @param  object $school     	Our Wonde school
     * @param  boolean $force     	Should we ignore our 50% size check and force an import
     * @return void
     */
    protected static function handlePossibilityForce($scheduleLog, $endPoint, $log, $school, $force = false, $client = false)
    {
        $option = new Option;
        $oldCount = $option->loadOption('mis_import_'.$endPoint->plural.'_count');
        $oldCount = (count($oldCount) > 0) ? $oldCount->text : 0;
        if($client){
            $newCount = count($client->attendanceCodes->all([''])->array);
        }else{
            $newCount = $school->counts->all([$endPoint->plural])->array;
            foreach ($newCount as $obj) {
                $newCount = $obj->data->count;
            }
        }
        
        $option->saveOption('mis_import_'.$endPoint->plural.'_count', ['text' => $newCount]);
        if (!$force) {
            if (($oldCount / 2) > $newCount) {
                $last = $scheduleLog->orderBy('updated_at', 'desc')->skip(1)->take(1)->get();
                if (count($last) == 0 || $last[0]->comment != 'aborted') {
                    $email = new Email;
                    $msg = 'The ' . config('app.school_title') .  config('app.system_name') .' has aborted the '.$endPoint->singular.' import. The '.$endPoint->singular.' import was less than half the number of existing '.$endPoint->plural.'.';
                    $result = $email->send(ucfirst($endPoint->singular).' Import Warning', $msg, ['emails' => [config('mail.error_address')]]);
                }
                $log->update(['comment' => 'aborted']);
                die('Aborted: '.ucfirst($endPoint->singular).' import is less than half of our current '.$endPoint->plural . PHP_EOL);
            }
        }
    }

    /**
     * Process data and run a function from the child class
     *  
     * @param 	array $data          	Array of records we want to process
     * @param 	class $child         	The class that this function is called from
     * @param 	date $last           	Most recent schedule log for this command
     * @param 	object $school 			Our Wonde school
     * @param 	object $endPoint		The data that we want to get from Wonde
     * @param 	string $comparison 		Which field should be compared when searching for 
     * @return 	function
     */
    protected static function processData($data, $child, $last, $school, $endPoint, $comparison)
    {
        foreach ($data as $obj) {
            $loc = array_search($obj->data[$comparison], $child->existing->lists($comparison)->toArray());
            if ($loc === false) {
                $child->create($obj);
            } else {
                $child->update($obj, $loc);
            }
        }

        if ($child->option('update') && $last) {
            foreach ($school->deletions->all([], ['type' => $endPoint->singular, 'created_after' => $last->updated_at]) as $deletion) {
                $child->ifExistsDelete($deletion->id);
            }
        } else {
            foreach ($child->existing as $object) {
                $child->delete($object);
            }
        }
    }
}
