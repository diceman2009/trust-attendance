<?php

namespace App\Console;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\MISAttendances::class,
        Commands\MISStudent::class,
        Commands\MISAttendanceCodes::class,
        Commands\MISImport::class,
        Commands\MISAttendance::class,
    ];

    /**
     * Staggers time based on the env APP_CRONJOB_STAGGER value
     * 
     * @param  $start (time to be staggered)
     * @return String in format HH:MM
     */
    private function staggerTime($start)
    {
        // $size = config('app.cronjob_stagger_gap');
        // $stagger = config('app.cronjob_stagger');
        // if ($stagger == 1) {
        //     return $start;
        // }
        // return Carbon::parse($start)->addMinutes($stagger * $size)->format('H:i');
    }

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule){

    }
}
