<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{

    /**
     * Student relationship
     * 
     * @return Students
     */
    public function students()
    {
        return $this->hasMany('App\Student', 'school', 'wonde_id');
    }

    /**
     * Default sort order queryscope
     * 
     * @param  $query
     * @return Builder
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('name', 'asc');
    }


    /**
     * Primary School queryscope
     * 
     * @param  $query
     * @return Builder
     */
    public function scopePrimary($query)
    {
        return $query->where('phase', '=', 'primary');
    }


    /**
     * Secondary School queryscope
     * 
     * @param  $query
     * @return Builder
     */
    public function scopeSecondary($query)
    {
        return $query->where('phase', '=', 'secondary');
    }


    /**
     * Secondary School queryscope
     * 
     * @param  $query
     * @return Builder
     */
    public function scope($query)
    {
    //
        return $query->where('phase', '!=', 'secondary')->orWhere('phase', '!=', 'primary');
    }
}
