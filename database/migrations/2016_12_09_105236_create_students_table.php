<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('wonde_id');
            $table->string('username', 125);
            $table->string('upn', 20);
            $table->integer('year');
            $table->string('tutor', 20);
            $table->string('sex', 1);
            $table->string('forename', 125);
            $table->string('surname', 125);
            $table->string('ethnicity', 10);
            $table->string('school'); 
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('students');
    }
}
