<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('school_id')->unsigned();
            $table->string('student_id', 15);
            $table->string('attendance_mark', 15);
            $table->string('session', 2);
            $table->date('date');

            $table->foreign('school_id')
                  ->references('id')
                  ->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendance', function($table) {
            $table->dropForeign(['school_id']);
        });
        Schema::drop('attendance');
        //Schema::drop('attendance_summary');
        //Schema::drop('attendance_codes');
    }
}
