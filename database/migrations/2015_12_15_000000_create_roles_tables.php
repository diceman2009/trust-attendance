<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id')->unsigned();;
            $table->string('name', 50)->unique();
            $table->string('label')->unique();
        });

        DB::table('roles')->insert([
            ['id' => 1,     'name' => 'super_admin',      'label' => 'Super Admin'],
            ['id' => 2,     'name' => 'admin',            'label' => 'Admin'],
            ['id' => 3,     'name' => 'technican',        'label' => 'Technician'],
            ['id' => 4,     'name' => 'staff',            'label' => 'Staff']
        ]);

        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id')->unsigned();;
            $table->string('name', 50)->unique();
            $table->string('label')->unique();
        });

        DB::table('permissions')->insert([
            ['id' => 1,     'name' => 'manage_users',              'label' => 'Can edit users'],
            ['id' => 2,     'name' => 'edit_settings',             'label' => 'Can change system settings'],
            ['id' => 3,     'name' => 'view_system_info',          'label' => 'Can view the system info on the about modal'],
            ['id' => 4,     'name' => 'view_all_orders',           'label' => 'Can view all orders'],
            ['id' => 5,     'name' => 'impersonate_admin',         'label' => 'Can impersonate users with the Admin role'],
            ['id' => 6,     'name' => 'impersonate_technician',     'label' => 'Can impersonate users with the Technician role'],
            ['id' => 7,     'name' => 'impersonate_staff',          'label' => 'Can impersonate users with the Staff role']
        ]);

        Schema::create('permission_role', function (Blueprint $table) {
            $table->integer('permission_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('permission_id')
                  ->references('id')
                  ->on('permissions')
                  ->onDelete('cascade');

            $table->foreign('role_id')
                  ->references('id')
                  ->on('roles')
                  ->onDelete('cascade');
        });

        DB::table('permission_role')->insert([
            ['role_id' => 1, 'permission_id' => 1],
            ['role_id' => 1, 'permission_id' => 2],
            ['role_id' => 1, 'permission_id' => 3],
            ['role_id' => 1, 'permission_id' => 4],
            ['role_id' => 1, 'permission_id' => 5],
            ['role_id' => 1, 'permission_id' => 6],
            ['role_id' => 1, 'permission_id' => 7],
            ['role_id' => 2, 'permission_id' => 1],
            ['role_id' => 2, 'permission_id' => 2],
            ['role_id' => 2, 'permission_id' => 3],
            ['role_id' => 2, 'permission_id' => 4],
            ['role_id' => 2, 'permission_id' => 6],
            ['role_id' => 2, 'permission_id' => 7],
            ['role_id' => 3, 'permission_id' => 4],
            ['role_id' => 3, 'permission_id' => 7]            
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET foreign_key_checks = 0');
        Schema::drop('permission_role');
        Schema::drop('roles');
        Schema::drop('permissions');
        DB::statement('SET foreign_key_checks = 1');
    }
}
