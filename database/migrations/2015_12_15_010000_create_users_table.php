<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->string('forename', 125);
            $table->string('surname', 125);
            $table->string('username', 125);
            $table->string('miscode', 125);
            $table->string('email', 200);
            $table->string('password', 60);
            $table->boolean('created_via_sso')->default(false);
            $table->boolean('allow_sso')->default(false);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('role_id')
                  ->references('id')
                  ->on('roles');
        });

        DB::table('users')->insert([
            [
                'role_id'   => 1,
                'forename'  => 'Founder',
                'surname'   => 'User',
                'username'  => 'nine_superadmin',
                'email'     => 'support@appnine.co.uk',
                'password'  => bcrypt('Password1'),
                'miscode'    => 'STC',
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
