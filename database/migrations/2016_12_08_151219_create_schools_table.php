<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 100)->unique();
            $table->string('fullname', 200)->unique();
            $table->string('description', 500)->nullable();
            $table->string('wonde_id', 15)->nullable();
            $table->string('phase', 50);
            $table->boolean('attendance')->default(false);
            $table->boolean('behaviour')->default(false);
            $table->timestamps();
        });

        DB::table('schools')->insert([
            [
                'id'   => 3,
                'name'  => 'Cockshut Hill',
                'fullname'   => 'Cockshut Hill School',
                'wonde_id'     => 'A418876919',
                'phase'  => 'secondary',
                'attendance' => true
            ]
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('schools');
    }
}
