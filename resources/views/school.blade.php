@extends(isset($onlyContent) && $onlyContent ? 'layouts.blank' : 'layouts.app')

@section('content')

<div class="row analysis-top" style="margin-top:20px;">
    <div class="col-sm-8">
        <div class="form-inline">
            <select id="report-select" class="form-control select select-primary select-sm">
                <optgroup label="{{ config('app.detention_title') }}">
                    <option value="0" @if ($reportId == '0') selected @endif>Year 7 Summary</option>
                    <option value="0" @if ($reportId == '1') selected @endif>Year 8 Summary</option>
                    <option value="0" @if ($reportId == '2') selected @endif>Year 9 Summary</option>
                    <option value="0" @if ($reportId == '3') selected @endif>Year 10 Summary</option>
                    <option value="0" @if ($reportId == '4') selected @endif>Year 11 Summary</option>
                    <option value="0" @if ($reportId == '5') selected @endif>Year 12 Summary</option>
                </optgroup>
            </select>
            <select id="date-select" class="form-control select select-primary select-sm">
                <option value="0" @if ($dateId == '0') selected @endif>This Academic Year</option>
                <option value="2" @if ($dateId == '2') selected @endif>Today</option>
                <option value="3" @if ($dateId == '3') selected @endif>Yesterday</option>
                <option value="4" @if ($dateId == '4') selected @endif>Past Week</option>
                <option value="5" @if ($dateId == '5') selected @endif>Past Month</option>
                <option value="6" @if ($dateId == '6') selected @endif>Past 3 Months</option>
                <option value="7" @if ($dateId == '7') selected @endif>Past 6 Months</option>
                <option value="8" @if ($dateId == '8') selected @endif>Past Year</option>
                <option value="9" @if ($dateId == '9') selected @endif>Past 2 Years</option>
            </select>
        </div>
    </div>
    <div class="col-sm-4 right-buttons">
        <button id="export-button" class="btn btn-sm btn-info pull-right" style="margin-left:6px;">
            <span class="fui-export"></span> Export
        </button>
        <button class="btn btn-sm btn-info pull-right page-print-button" style="margin-left:6px;">
            <span class="glyphicon glyphicon-print"></span> Print
        </button>        
    </div>
    <div class="col-sm-8">
        <h6>{{ $school->fullname }}</h6>
        <div class="well well-sm" style="margin-bottom: 0;">
            <div class="row">
                <div class="col-md-12">
                    <table class="table sortabletable datatable">
                        <thead>
                            <tr>
                                <th class="firstcol">Name</th>
                                <th class="datacol">Gender</th>
                                <th class="datacol">Tutor</th>
                                @foreach ($attendanceCodes as $code)
                                <th class="datacol">
                                	{{ucfirst(strtolower($code))}}
                                </th>
                                @endforeach                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach( $students as $student )
                                @if($student->display != 'no')
                                <tr>
                                    <td>{{ $student->fullName }}</td>
                                    <td class="datacol">{{ $student->sex }}</td>
                                    <td class="datacol">{{ $student->tutor }}</td>
                                    @foreach( $student->summary as $summary )
                                        <td class="datacol">{{ $summary }}%</td>
                                    @endforeach
                                </tr>  
                                @endif                              
                            @endforeach               
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <h6>Past Week Overview</h6>
        <div class="well well-sm" style="margin-bottom: 0;">
            <canvas id="myChart2"></canvas>
            <script>
                var totals = {!! json_encode($totals) !!};
                var dataaa = {
                    labels: [
                        "Present",
                        "Authorised",
                        "Unauthorised"
                    ],
                    datasets: [
                        {
                            data: [totals["PRESENT"], totals["AUTHORISED"],totals["UNAUTHORISED"]],
                            backgroundColor: [
                                "#FF6384",
                                "#36A2EB",
                                "#FFCE56"
                            ],
                            hoverBackgroundColor: [
                                "#FF6384",
                                "#36A2EB",
                                "#FFCE56"
                            ]
                        }]
                };
                var ctx = $("#myChart2");

                var myDoughnutChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: dataaa,
                     options: {
                        legend: {
                            position: 'right',
                            labels: {
                                boxWidth: 12,
                                fontFamily: "'Lato', Helvetica, Arial, sans-serif",
                                padding: 19
                            }
                        },
                        layout: {
                            padding: 3
                        }
                    }
                });

            </script>
        </div>
    </div>
</div>


<script>
    @minify('js')

        $("select").select2({dropdownCssClass: 'dropdown-inverse'});



    @endminify
</script>

@endsection