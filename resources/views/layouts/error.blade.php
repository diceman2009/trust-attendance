<!DOCTYPE html>
<html lang="en">
	<head>
		@include('layouts.partials.head')
	</head>
	<body>
		@include('layouts.partials.nojs')
		
		@yield('content')
	</body>
</html>