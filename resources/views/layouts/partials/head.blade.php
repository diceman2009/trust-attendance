<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Trust Picture</title>

<!-- <link rel="icon" type="image/png" sizes="16x16" href="{{ url('img/favicon-16x16.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ url('img/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ url('img/favicon-96x96.png') }}">
<link rel="apple-touch-icon" sizes="57x57" href="{{ url('img/apple-icon-57x57.png') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ url('img/apple-icon-60x60.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ url('img/apple-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ url('img/apple-icon-76x76.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ url('img/apple-icon-114x114.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ url('img/apple-icon-120x120.png') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ url('img/apple-icon-144x144.png') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ url('img/apple-icon-152x152.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ url('img/apple-icon-180x180.png') }}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ url('img/ms-icon-144x144.png') }}">
<meta name="theme-color" content="#ffffff"> -->

<link href="{{ url('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('css/flat-ui.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('css/datepicker3.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url(elixir('css/custom.css')) }}" rel="stylesheet" type="text/css">
<link href="{{ url(elixir('css/trust.css')) }}" rel="stylesheet" type="text/css">

@if (!Request::is('login'))
	<link href="{{ url(elixir('css/print.css')) }}" media="print" rel="stylesheet">
@endif

@if (File::exists(public_path('/custom/override.css')))
	<link href="{{ url('custom/override.css') }}" rel="stylesheet" type="text/css">
@endif

{{-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries,p also excanvas.js for support of html5 canvas --}}
<!--[if lte IE 8]>
	<link href="{{ url(elixir('css/ie8tweaks.css')) }}" rel="stylesheet" type="text/css">
	<script src="{{ url('js/html5shiv.js') }}"></script>
	<script src="{{ url('js/respond.min.js') }}"></script>
	<script src="{{ url('js/excanvas.min.js') }}"></script>
<![endif]-->
<!--[if IE]>
	<link href="{{ url(elixir('css/ietweaks.css')) }}" rel="stylesheet" type="text/css">
<![endif]-->