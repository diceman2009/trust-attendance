{{-- Impersonation overlay box --}}

@if(session()->get('impersonate_actual'))
    <div id="impersonate-box">
        <div class="alert alert-sm alert-danger" role="alert">
            <p class="title">
                <span class="fui-alert-circle" aria-hidden="true"></span> Impersonating {{ Auth::user()->fullname }}
            </p>
            <button class="btn btn-sm btn-info modallink" data-urlpath="{{ url('modal/impersonate') }}"><span class="fui-eye"></span> Change User</button>
            &nbsp; <button id="unimpersonate-button" class="btn btn-sm btn-danger"><span class="fui-cross-circle"></span> Un-impersonate</button>
        </div>
    </div>
    <div id="impersonate-margin"></div>

    <script>
        @minify('js')

        $(document).ready(function() {
            $('#impersonate-box').transition({ y: '-50px', delay: 250, opacity: 1, duration: 1000, queue:false });
            $('#impersonate-box #unimpersonate-button').click(function() {
                spinner.showLoader();
                document.location.href = "{{ url('unimpersonate') }}";
            });
        });
        
        @endminify
    </script>
@endif