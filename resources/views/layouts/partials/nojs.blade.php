<noscript>
	<div id="dashboard-content" class="container no-js">
		<br/>
		<div class="well well-sm">
			<p class="no-results">
				<span class="fui-info-circle icon-sized"></span> This website requires javascript to be enabled.
			</p>
		</div>
	</div>
</noscript>