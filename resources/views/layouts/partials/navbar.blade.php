{{-- Page Navigation bar --}}
<div class="navbar navbar-inverse navbar-embossed navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<a href="{{ url('home') }}" class="navbar-brand" data-toggle="collapse" data-target=".in">
				<span class="glyphicon glyphicon-education icon-pad"></span>
				Trust Picture
			</a>
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>

		<div class="navbar-collapse collapse" id="navbar-main">
			<ul class="nav navbar-nav">
				<!-- <li data-urlpath="{{ url('overview') }}" data-toggle="collapse" data-target=".in">
					<a href="{{ url('overview') }}">Overview</a>
				</li> -->
				<li data-urlpath="{{ url('attendance') }}" data-toggle="collapse" data-target=".in">
					<a href="{{ url('attendance') }}">Attendance</a>
				</li>
				<li data-urlpath="{{ url('behaviour') }}" data-toggle="collapse" data-target=".in">
					<a href="{{ url('behaviour') }}">Behaviour</a>
				</li>
			</ul>

			<ul class="nav navbar-nav navbar-right">	

				{{-- Schools Dropdown --}}
				<li id="main-group-list-dropdown" class="dropdown scrollable-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">School &nbsp;<span class="glyphicon glyphicon-triangle-bottom"></span></a> 
					<ul id="group-list" class="dropdown-menu" role="menu">
						@if (count($primarySchools) > 0)
							<li class="dropdown-title">PRIMARY</li>
							@foreach ($primarySchools as $school)
								<li class="avoid-active" data-urlpath="{{ url('school', $school->id) }}" data-toggle="collapse" data-target=".in">
									<a href="{{ url('school', $school->id) }}">{{ $school->name }}</a>
								</li>
							@endforeach
							@if (count($secondarySchools) > 0)
								<li class="divider"></li>
							@endif
						@endif
						@if (count($secondarySchools) > 0)
							<li class="dropdown-title">SECONDARY</li>
							@foreach ($secondarySchools as $school)
								<li class="avoid-active" data-urlpath="{{ url('school', $school->id) }}" data-toggle="collapse" data-target=".in">
									<a href="{{ url('school', $school->id) }}">{{ $school->name }}</a>
								</li>
							@endforeach
						@endif
					</ul>
				</li>

				{{-- Extras Dropdown --}}
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fui-list"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a href="{{ url('logout') }}"><span class="fui-exit icon-pad"></span>  Log out</a>
						</li>
						@if (!session('sso'))
							<li data-urlpath="{{ url('modal/changepassword') }}" data-linktype="modal" data-toggle="collapse" data-target=".in">
								<a href="#"><span class="fui-lock icon-pad"></span> Change Password</a>
							</li>
						@endif
						@if(Gate::allows('impersonate_admin') || Gate::allows('impersonate_staff'))
							<li data-urlpath="{{ url('modal/impersonate') }}" data-linktype="modal" data-toggle="collapse" data-target=".in">
								<a href="#"><span class="fui-eye icon-pad"></span> Impersonate User</a>
							</li>
						@endif
						<li data-urlpath="{{ url('modal/about') }}" data-linktype="modal" data-toggle="collapse" data-target=".in">
							<a href="#"><span class="fui-info-circle icon-pad"></span> About</a>
						</li>
						@if(Gate::allows('edit_settings') || Gate::allows('manage_users'))
							<li class="divider"></li>
						@endif	
						@can('manage_users')
							<li data-urlpath="{{ url('modal/users') }}" data-linktype="modal" data-toggle="collapse" data-target=".in">
								<a href="#"><span class="fui-user icon-pad"></span> Manage Users</a>
							</li>
						@endcan
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>

<script>
	@minify('js')
	
	navbar = {};
	navbar.baseURL = "{{ url('/') }}";

	navbar.date = '';
	navbar.term = '';

	$('.navbar #nav-search-form').on("submit", function(e) {
		if ($('#nav-search-form #search-term').val() != '') {
			$('.navbar-nav li').removeClass('active');
			app.loadPageHTML($(this).attr('action') + '/' + encodeURIComponent($('#nav-search-form #search-term').val()) + '/' + navbar.date, true);
			$('#nav-search-form #search-term').blur();
			if ($('.navbar-toggle').css('display') != 'none') {
				$('.navbar-collapse').collapse('hide');
			}
		}
		e.preventDefault();
	});

	@endminify
</script>