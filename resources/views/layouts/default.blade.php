<!DOCTYPE html>
<html lang="en">
	<head>
		@include('layouts.partials.head')
	</head>
	<body>
		<script src="{{ url(elixir('js/main.js')) }}"></script>

		@include('layouts.partials.nojs')
		
		@yield('content')

		@include('layouts.partials.google-analytics')
	</body>
</html>