<!DOCTYPE html>
<html lang="en">
	<head>
		@include('layouts.partials.head')
	</head>
	<body>
		<script src="{{ url(elixir('js/main.js')) }}"></script>
		<script src="{{ url('js/typeahead.bundle.min.js') }}"></script>
		<script src="{{ url('js/jquery.fileupload.min.js') }}"></script>
		<script src="{{ url('js/jquery.iframe-transport.min.js') }}"></script>
		<script src="{{ url('js/Chart.min.js') }}"></script>

		@include('layouts.partials.navbar')
		@include('layouts.partials.nojs')
		
		<div id="main-content" role="main" class="container">
			@yield('content')
		</div>

		@include('layouts.partials.impersonate')
		@include('layouts.partials.google-analytics')
	</body>
</html>