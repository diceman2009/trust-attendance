{{-- Login Page --}}

@extends('layouts.default')

@section('content')

<div class="login-page">
	<div class="container" role="main">
		@if (File::exists(public_path('/custom/logo.jpg')))
			<div id="school-logo">
				<img src="{{ url('/custom/logo.jpg') }}" alt="School logo" />
			</div>
		@endif
		<form id="login-form" method="POST" action="{{ url('/login') }}">
			<div id="login-box" class="login-form col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
				<div id="box-top">
					<span class="glyphicon glyphicon-education icon-pad"></span>
					Trust Picture
				</div>
				<div class="inner-form">
					<p id="feedback"></p>
					<div class="form-group">
						<input type="text" class="form-control login-field" value="" placeholder="Username" id="login-name" name="username" />
						<label class="login-field-icon fui-user" for="login-name"></label>
					</div>
					<div class="form-group">
						<input type="password" class="form-control login-field" value="" placeholder="Password" id="login-pass" name="password" />
						<label class="login-field-icon fui-lock" for="login-pass"></label>
					</div>
					<button id="login-button" class="btn btn-info btn-block" type="submit">
						<span class="glyphicon glyphicon-log-in"></span>&nbsp; Log in
					</button>
				</div>
			</div>
		</form>
		<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
			<p class="alt-login-links">
				@if (config('app.sso_firefly_enabled') == true)
					<a href="{{ url('/login/sso/firefly') }}" class="btn btn-sm btn-primary alt-login-link">
						<span class="glyphicon glyphicon-log-in"></span>&nbsp; Log in via {{ config('app.sso_firefly_friendly_title') }}
					</a>
					<br/>
				@endif
				@if (config('app.sso_realsmart_enabled') == true)
					<a href="{{ url('/login/sso/realsmart') }}" class="btn btn-sm btn-primary alt-login-link">
						<span class="glyphicon glyphicon-log-in"></span>&nbsp; Log in via {{ config('app.sso_realsmart_friendly_title') }}
					</a>
					<br/>
				@endif
			</p>
		</div>
	</div>
</div>

<script>
	@minify('js')
	
	$(document).ready(function() {
		$('#login-form').submit(function(e) {
			app.submitForm(this, {
				beforeSubmitFunction: function() {
					$('input').blur();
					$('#feedback').hide();
					$('#login-box').transition({ scale: 0.98 });
				},
				buttonErrorText: 'Attempt Failed',
				buttonSuccessText: '&nbsp;',
				success: function(form, ret) {
					$('body').transition({ opacity: 0, duration: 175, complete: function(){ window.location.href = ret.data.intended; }});
					$('#login-button').removeClass('btn-success');
				},
				successAfterButton: function() {
					$('#login-button').removeClass('btn-success');
				},
				error: function(form, ret) {
					$('#login-box').transition({ scale: 1 }).transition({ x: -17, duration: 140 }).transition({ x: 17, duration: 140 }).transition({ x: -9, duration: 130 }).transition({ x: 6, duration: 130 }).transition({ x: 0, duration: 130 });
					if (ret.responseJSON && ret.responseJSON.data) {
						if (!isNaN(ret.responseJSON.message)) {
							startCounter(ret.responseJSON.message);
						}
					}
				}
			});
			e.preventDefault();
		});

		counterTimeout = null;
		function startCounter(seconds) {
			if (counterTimeout) {
				clearTimeout(counterTimeout);
			}
			if (seconds > 0) {
				var grammar = (seconds == 1) ? 'second' : 'seconds';
				$('#feedback').html('<span class="fui-alert-circle"></span> Too many login attempts. Please try again in ' + seconds + ' ' + grammar + '.').show();
				counterTimeout = setTimeout(function() { 
					startCounter(seconds - 1); 
				}, 1000);
			} else {
				$('#feedback').hide();
			}
		}

		if (typeof bowser == 'undefined' || bowser.msie && bowser.version <= 7) {
			alert('Warning: This browser is not supported.');
		}
		if (navigator.cookieEnabled) { 
			cookiesEnabled = true; 
		} else {
			document.cookie = 'cookietest=1';
			var cookiesEnabled = (document.cookie.indexOf('cookietest=') != -1) ? true : false;
			document.cookie = 'cookietest=1; expires=Thu, 01-Jan-1970 00:00:01 GMT';
		}
		if (!cookiesEnabled) {
			alert('Cookies must be enabled to login on this website.');
		}

		$('.login-page').transition({ opacity: 1, duration: 400 });
	});

	@endminify
</script>

@endsection