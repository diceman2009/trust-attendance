{{-- Used for showing validation errors --}}
<script>
	$(document).ready(function() {
		$('#error-box').transition({ opacity: 1, duration: 300, queue:false });
	});
</script>

@if (count($errors) > 0)
	<div id="error-box" class="alert alert-danger" style="opacity: 0;"> 
		<p class="title">
			<span class="fui-alert-circle"></span> The following validation errors occurred:
		</p>
		<ul>
			@foreach ($errors->all() as $error)
				@if ($error != '')
					<li>{{ $error }}</li>
				@endif
			@endforeach
		</ul>
	</div>
@endif