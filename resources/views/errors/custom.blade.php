@extends('layouts.error')

@section('content')

<div id="dashboard-content" class="container">
	<br/>
	<div class="well well-sm">
		<p class="no-results">
			<span class="fui-info-circle icon-sized"></span> {{ $error }}
		</p>
	</div>
</div>

@endsection