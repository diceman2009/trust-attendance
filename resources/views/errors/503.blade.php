@extends('layouts.error')

@section('content')

<div id="dashboard-content" class="container">
	<br/>
	<div class="well well-sm">
		<p class="no-results">
			<span class="fui-info-circle icon-sized"></span> The system is currently undergoing maintenance, please try again later.
		</p>
	</div>
</div>

@endsection