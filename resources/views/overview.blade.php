@extends(isset($onlyContent) && $onlyContent ? 'layouts.blank' : 'layouts.app')

@section('content')

<div class="row">
    <div class="col-md-5">
        <h6>Academic Year Summary</h6>
        <div class="well well-sm">
            <table class="table sortabletable datatable">
                <thead>
                    <tr>
                        <th class="firstcol">School Name</th>
                        <th class="datacol strip">% Present</th>
                        <th class="datacol">% Auth</th>
                        <th class="datacol strip">% Unauth</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($attendance as $row)
                        <tr>
                            <td class="firstcol">
                                <a href="{{ url('school', $row->school_id) }}" class="namelink pagelink">{{ $row->name }}</a>
                            </td>
                            <td class="datacol strip">@if ($row->sum > 0) {{ number_format(($row->present / $row->sum) * 100, 2) }} @else 0.00 @endif</td>
                            <td class="datacol">@if ($row->sum > 0) {{ number_format(($row->auth / $row->sum) * 100, 2) }} @else 0.00 @endif</td>
                            <td class="datacol strip">@if ($row->sum > 0) {{ number_format(($row->unauth / $row->sum) * 100, 2) }} @else 0.00 @endif</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-7">
        <h6>Past Month by Week</h6>
        <div class="well well-sm" style="margin-bottom: 0;">
            <canvas id="myChart" width="400" height="210"></canvas>
            <script>
                var fakeData = {
                    labels: ["14/11", "21/11", "28/11", "05/12"],
                    datasets: [
                        {
                            label: "Cockshut Hill",
                            borderColor: "rgba(171, 36, 53, 0.7)",
                            backgroundColor: "rgba(171, 36, 53, 0.7)",
                            fill: false,
                            lineTension: 0,
                            data: [90.22, 87.34, 91.12, 89.04]
                        },{
                            label: "Erdington Hall",
                            borderColor: "rgba(37, 45, 100, 0.7)",
                            backgroundColor: "rgba(37, 45, 100, 0.7)",
                            fill: false,
                            lineTension: 0,
                            data: [94.78, 92.34, 91.12, 89.77]
                        },{
                            label: "Lyndon",
                            borderColor: "rgba(120, 148, 204, 0.7)",
                            backgroundColor: "rgba(120, 148, 204, 0.7)",
                            fill: false,
                            lineTension: 0,
                            data: [91.73, 91.34, 90.44, 92.04]
                        },{
                            label: "Ninestiles",
                            borderColor: "rgba(0, 160, 210, 0.7)",
                            backgroundColor: "rgba(0, 160, 210, 0.7)",
                            fill: false,
                            lineTension: 0,
                            data: [93.07, 85.34, 90.03, 90.34]
                        },{
                            label: "Pegasus",
                            borderColor: "rgba(115, 80, 160, 0.7)",
                            backgroundColor: "rgba(115, 80, 160, 0.7)",
                            fill: false,
                            lineTension: 0,
                            data: [92.34, 88.90, 87.44, 88.67]
                        },{
                            label: "The Oaklands",
                            borderColor: "rgba(46, 107, 73, 0.7)",
                            backgroundColor: "rgba(46, 107, 73, 0.7)",
                            fill: false,
                            lineTension: 0,
                            data: [87.78, 89.04, 90.55, 87.55]
                        },{
                            label: "Yarnfield",
                            borderColor: "rgba(35, 83, 165, 0.7)",
                            backgroundColor: "rgba(35, 83, 165, 0.7)",
                            fill: false,
                            lineTension: 0,
                            data: [93.44, 90.44, 92.14, 92.68]
                        }
                    ]
                };
                var ctx = $("#myChart");
                var myLineChart = Chart.Bar(ctx, {
                    data: fakeData,
                    options: {
                        legend: {
                            position: 'right',
                            labels: {
                                boxWidth: 12,
                                fontFamily: "'Lato', Helvetica, Arial, sans-serif",
                                padding: 19
                            }
                        },
                        layout: {
                            padding: 3
                        }
                    }
                });
            </script>
        </div>
    </div>
</div>

<!-- <div class="row">
    <div class="col-xs-12">
        <h6>BFL Summary</h6>
        <div class="well well-sm">
            
        </div>
    </div>
</div> -->

@endsection