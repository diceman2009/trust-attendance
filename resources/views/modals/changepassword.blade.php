{{-- Change Password Modal --}}
<div id="modal-outer" class="modal fade" tabindex="-1" role="dialog" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h5 class="modal-title">Change Password</h5>
			</div>

			<form id="change-password-form" class="close-warning" action="{{ url('/modal/changepassword') }}" method="POST" autocomplete="off">
				<div class="modal-body">
					<div class="innerarea-form-modal">
						<div id="validation-errors"></div>
						<div class="alert alert-sm alert-info" role="alert">
							<p class="title">
								<span class="fui-info-circle" aria-hidden="true"></span> Your new password must:
							</p>
							<ul>
								<li>Be at least 8 characters in length.</li>
								<li>Contain at least 1 number.</li>
								<li>Contain at least 1 capital letter.</li>
								<li>Contain at least 1 lowercase letter.</li>
							</ul>
						</div>
						<div class="row">
							<div class="form-group maxlength-no-button">
								<label class="control-label col-sm-4">Current Password</label>
								<div class="col-sm-8">
									<input type="password" name="current_password" id="current_password" class="form-control input-sm" value="" maxlength="125" />
								</div>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="form-group maxlength-no-button">
								<label class="control-label col-sm-4">New Password</label>
								<div class="col-sm-8">
									<input type="password" name="new_password" id="new_password" class="form-control input-sm" value="" maxlength="125" />
								</div>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="form-group maxlength-no-button">
								<label class="control-label col-sm-4">Repeat New Password</label>
								<div class="col-sm-8">
									<input type="password" name="new_password_confirmation" id="new_password_confirmation" class="form-control input-sm" value="" maxlength="125" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>

			<div class="modal-footer">
				<button type="button" id="change-password-submit" class="btn btn-sm btn-info" style="min-width: 130px;">
					<span class="fui-check"></span> Save Changes
				</button>
			</div>
		</div>
	</div>
</div>

<script>
	@minify('js')

	$('.modal #change-password-submit').click(function() {
		$('.modal #change-password-form').submit();
	});

	$('.modal #change-password-form').submit(function(e) {
		app.submitModalForm(this, {
			submitRef: '.modal #change-password-submit',
			buttonSuccessText: 'Password Changed'
		});
		e.preventDefault();
	});

	@endminify
</script>