<div id='modal-settings-info'>
	<div id="modal-outer" class="modal fade" tabindex="-1" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<h5 class="modal-title">About</h5>
				</div>
				<div class="modal-body">
					@can('view_system_info')
						<div class="panel panel-default">
							<div class="panel-heading">System Information</div>
							<table class="table">
								<tbody><tr>
									<td class="col-xs-6">Version:</td>
									<td class="col-xs-6">{{ config('app.version') }}</td>
								</tr>
								<tr>
									<td class="col-xs-6">School:</td>
									<td class="col-xs-6">{{ config('app.school_title') }}</td>
								</tr>
								@if (count($misImport) > 0)
									<tr>
										<td class="col-xs-6">Last MIS Import:</td>
										<td class="col-xs-6">{{ $misImport }}</td>
									</tr>
								@endif
							</tbody></table>
						</div>
					@endcan
					<div class="panel panel-default" style="margin-bottom: 0;">
						<div class="panel-heading">User Information</div>
						<table class="table">
							<tbody><tr>
								<td class="col-xs-6">Username:</td>
								<td class="col-xs-6">{{ $user->username }}</td>
							</tr>
							<tr>
								<td class="col-xs-6">Fullname:</td>
								<td class="col-xs-6">{{ $user->forename }} {{ $user->surname }}</td>
							</tr>
							<tr>
								<td class="col-xs-6">Role:</td>
								<td class="col-xs-6">{{ $user->role->label }}</td>
							</tr>
							<tr>
								<td class="col-xs-6">Email Address:</td>
								<td class="col-xs-6">{{ $user->email }}</td>
							</tr>
						</tbody></table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>