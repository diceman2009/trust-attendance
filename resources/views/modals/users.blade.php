{{-- User Management Modal --}}
<div id="modal-outer" class="modal fade" tabindex="-1" role="dialog" aria-hidden="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h5 class="modal-title">Manage Users</h5>
			</div>

			<div class="modal-body">
				@if (count($users) > 0)
					<div class="modal-table-scroller">
						<table class="table sortabletable listtable">
							<thead>
								<tr>
									<th width="9%" class="no-hover"></th>
									<th>Username</th>
									<th>Forename</th>
									<th>Surname</th>
									<th>Role</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($users as $user)
									<tr>
										<td class="edit-user-col">
											<button type="button" class="btn btn-xs btn-primary pull-left edit-user-button" data-user-id="{{ $user->id }}">
												<span class="fui-new"></span>Edit
											</button>
										</td>
										<td>{{ $user->username }}</td>
										<td>{{ $user->forename }}</td>
										<td>{{ $user->surname }}</td>
										<td>{{ $user->role->label }}</td>
										<td>{{ $user->email }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				@else
					<div class="alert alert-sm alert-info empty-list" role="alert">
						<p class="title" style="padding-bottom: 0px;">
							<span class="fui-info-circle" aria-hidden="true"></span> No users exist.
						</p>
					</div>
				@endif
			</div>

			<div class="modal-footer">
				<button id="import-users-button" class="btn btn-sm btn-info" data-dismiss="modal">
					<span class="glyphicon glyphicon-import"></span> Import &amp; Export Users
				</button>
				<button id="add-user-button" class="btn btn-sm btn-info" data-dismiss="modal">
					<span class="fui-plus"></span> Create User
				</button>
			</div>
		</div>
	</div>
</div>

<script>
	@minify('js')

	if (!userManagement) {
		var userManagement = {};
	}
	app.rememberScroll(userManagement, '.modal .modal-table-scroller');
	app.rememberSort(userManagement, '.modal .sortabletable');
	
	$('.modal .edit-user-button').click(function() {
		app.modalDismiss("{{ url('/modal/users') }}/" + $(this).data('user-id') + "/edit");
	});

	$('.modal #import-users-button').click(function() {
		app.modalDismiss("{{ url('/modal/users/import') }}");
	});

	$('.modal #add-user-button').click(function() {
		app.modalDismiss("{{ url('/modal/users/create') }}");
	});

	@endminify
</script>