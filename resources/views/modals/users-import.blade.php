{{-- Import Users Modal --}}
<div id="modal-outer" class="modal fade" data-dismiss-follow="{{ url('/modal/users') }}" tabindex="-1" role="dialog" aria-hidden="false">
	<div id="usersimport-modal" class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h5 class="modal-title">Import &amp; Export Users</h5>
			</div>
			<div class="modal-body">
				<div class="innerarea-form-modal">
					<div class="alert alert-sm alert-info" role="alert" style="margin-bottom: 0px;">
						<p class="title">
							<span class="fui-info-circle" aria-hidden="true"></span> To import users a CSV file is required with the following columns:
						</p>
						<ul>
							<li>UID (<strong>Optional Column</strong>: required if you're updating an existing user - to find a users UID perform a user export)</li>
							<li>Username</li>
							<li>Forename</li>
							<li>Surname</li>
							<li>Role (<strong>Must match</strong> a system role: Staff, Privileged Staff or Admin)</li>
							<li>Email Address</li>
							@if (config('app.sso_firefly_enabled') || config('app.sso_realsmart_enabled'))
								<li>SSO Allowed (Should this user be allowed to single sign on)</li>
							@endif
							<li>Password (<strong>Optional Column</strong>: if left blank the user's password will not be changed)</li>
						</ul>
					</div>

					{{-- File input --}}
					<div class="btn btn-primary btn-sm fileinput-button">
						<span class="glyphicon glyphicon-import"></span> Import Users
						<input id="Filedata" type="file" name="Filedata" accept=".csv,text/csv">
					</div><div id="files-info" class="pull-right"></div>
					<button id="export-but" class="btn btn-sm btn-primary" style="margin-top: 5px; margin-left: 4px;">
						<span class="glyphicon glyphicon-export"></span> Export Users
					</button>
					<div id="progress" class="progress">
						<div class="progress-bar"></div>
					</div>
					<div id="files-info-error"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-info" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
	@minify('js')

	var settingsUsersImp = {
		'jqXHR': null,
		'importQueue': [],
		'importQueuePos': -1,
		'state': 'ready'
	};

	settingsUsersImp.jqXHR = null;
	settingsUsersImp.importQueue = [];
	settingsUsersImp.importQueuePos = -1;
	settingsUsersImp.state = "ready";

	$('#usersimport-modal #export-but').click(function() {
		window.open("{{ url('/modal/users/export') }}", '_blank');
	});

	$('#usersimport-modal #Filedata').fileupload({
		url: "{{ url('/modal/users/import') }}",
		dataType: 'json',
		method: 'POST',
		sequentialUploads: true,
		formData: { '_token': $('meta[name="csrf-token"]').attr('content') },
		add: function(e, data) {
			settingsUsersImp.jqXHR = data.submit();
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#usersimport-modal #progress .progress-bar').removeClass('no-transition').css('width', progress + '%');
			if (data.loaded == data.total) { $('#usersimport-modal #progress .progress-bar').css('width', '100%'); }
		},
		done: function(e, data) {
			if (data.result.status == 'error') {
				settingsUsersImp.state = 'complete';
				$('#usersimport-modal #files-info').html('');
				$('#usersimport-modal #progress .progress-bar').addClass('no-transition').css('width', '0px');
				if (data.result.data) {
					$('#usersimport-modal #files-info-error').prepend('<span class="error-msg">Error: ' + data.result.data.errors['Filedata'] + '</span><br/>');
				}
			} else {
				$('#usersimport-modal #progress .progress-bar').addClass('no-transition').css('width', '0px');
				settingsUsersImp.importQueue = data.result.data;
				settingsUsersImp.importQueuePos = 0;
				setTimeout(function(){ settingsUsersImp.importUsers(); }, 100);
			}
		},
		error: function (e, data) {
			settingsUsersImp.state = 'complete';
			$('#usersimport-modal #files-info').html('');
			$('#usersimport-modal #progress .progress-bar').addClass('no-transition').css('width', '0px');
			if (e.responseJSON && e.responseJSON.data) {
				$('#usersimport-modal #files-info-error').prepend('<span class="error-msg">Error: ' + e.responseJSON.data.errors['Filedata'] + '</span><br/>');
			}
		},
		change: function(e) {
			settingsUsersImp.importQueue = [];
			settingsUsersImp.importQueuePos = -1;
			data.cancelAllRequests();
			$('#usersimport-modal #files-info-error').html('');
			$('#usersimport-modal #files-info').html('Uploading');
			$('#usersimport-modal #progress .progress-bar').addClass('no-transition').css('width', '0px');
		},
		processQueue: [{
			action: 'validate',
			always: true,
			acceptFileTypes: /(\.|\/)(csv)$/i,
			maxFileSize: 2097152
		}],
		processfail: function(e, data) {
			$('#usersimport-modal #files-info-error').prepend('<span class="error-msg">' + data.files[0].error + ': ' + data.files[0].name + '</span><br/>');
		}
	});

	//LOOP THROUGH AND IMPORT USERS
	settingsUsersImp.importUsers = function() {
		settingsUsersImp.state = 'started';
		if (settingsUsersImp.importQueuePos < settingsUsersImp.importQueue.length) {
			var progress = parseInt(settingsUsersImp.importQueuePos / settingsUsersImp.importQueue.length * 100, 10);
			$('#usersimport-modal #progress .progress-bar').removeClass('no-transition').css('width', progress + '%');
			$('#usersimport-modal #files-info').html('Importing ' + (settingsUsersImp.importQueuePos + 1) + ' of ' + settingsUsersImp.importQueue.length);
			var user = settingsUsersImp.importQueue[settingsUsersImp.importQueuePos];
			var method = (user.uid != null && user.uid != '') ? 'update' : 'create';
			settingsUsersImp.saveUser(method, user);
		} else {
			settingsUsersImp.state = 'complete';
			$('#usersimport-modal #progress .progress-bar').css('width', '100%');
			$('#usersimport-modal #files-info').html('Importing Complete');
		}
	}

	//CREATE OR UPDATE A USER
	settingsUsersImp.saveUser = function(method, user) {
		var sendURL = "{{ url('/modal/users') }}";
		var pwd = (user.password) ? user.password : '';
		var sendObj = {
			'username': user.username,
			'password': pwd,
			'password_confirmation': pwd,
			'forename': user.forename,
			'surname': user.surname,
			'role': user.role,
			'miscode': user.mis_username,
			'email': user.email_address
		}
		if (user.sso_allowed == 1 || user.sso_allowed == 'Yes' || user.sso_allowed == 'yes' || user.sso_allowed == 'Y' || user.sso_allowed == 'y') {
			sendObj.sso = 1;
		}
		if (method == 'update') {
			sendObj._method = 'PATCH';
			sendURL += '/' + user.uid;
		}

		data.performRequest({
			method: 'POST',
			dataType: 'json',
			url: sendURL,
			data: sendObj,
			success: function(json) {
				settingsUsersImp.importQueuePos++;
				settingsUsersImp.importUsers();
			},
			error: function(e) {
				if (e.responseJSON && e.responseJSON.data) {
					var message = '';
					var errors = e.responseJSON.data.errors;
					$.each(errors, function(key, value) {
						if (value == 'Page not found') {
							value = 'This user does not exist.'
						}
						if (value instanceof Array) {
							for (var x = 0; x < value.length; x++) {
								message += value[x] + ' ';
							}
						} else {
							message += value + ' ';
						}
					});
					$('#usersimport-modal #files-info-error').prepend('<span class="error-msg">Row ' + (settingsUsersImp.importQueuePos + 1) + ' (' + sendObj.username + '): ' + message + '</span><br/>');
				} else {

				}
				settingsUsersImp.importQueuePos++;
				settingsUsersImp.importUsers();
			}
		});
	}

	//ON MODAL HIDE CONFIRM CLOSING IF UPLOADING AND CANCEL ALL UPLOADS
	$('#modal-outer').on('hide.bs.modal', function() {
		if (settingsUsersImp.state == 'started' && !confirm('Are you sure you want to close this modal, user importing will be aborted?')) { 
			e.preventDefault();
			e.stopPropagation();
			return false; 
		}
		settingsUsersImp.importQueue = [];
		settingsUsersImp.importQueuePos = -1;
		data.cancelAllRequests();
		$('#usersimport-modal #Filedata').fileupload('destroy');
		if (settingsUsersImp.jqXHR) { settingsUsersImp.jqXHR.abort(); }
	});

	@endminify
</script>