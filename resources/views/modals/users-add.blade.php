{{-- Add User Modal --}}
<div id="modal-outer" class="modal fade" data-dismiss-follow="{{ url('/modal/users') }}" tabindex="-1" role="dialog" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h5 class="modal-title">@if (isset($user->id)) Edit User @else Create User @endif</h5>
			</div>

			<form id="add-edit-user-form" class="close-warning" action="@if (isset($user->id)) {{ url('/modal/users', $user->id) }} @else {{ url('/modal/users') }} @endif" method="POST" autocomplete="off">
				@if (isset($user->id))
					{{ method_field('PATCH') }}
				@endif

				<div class="modal-body">
					<div class="innerarea-form-modal">
						<div id="validation-errors"></div>
						<div class="row">
							<div class="form-group maxlength-no-button">
								<label class="control-label col-sm-3">Username</label>
								<div class="col-sm-9">
									<input type="text" name="username" id="username" class="form-control input-sm" value="{{ $user->username or '' }}" maxlength="125" />
								</div>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="form-group maxlength-with-button">
								<label class="control-label col-sm-3">@if (isset($user->id)) New @endif Password</label>
								<div class="col-sm-9">
									<div class="input-group input-group-sm input-maxlength-3">
									  <input type="password" id="password" class="form-control" name="password" value="" maxlength="125" placeholder="@if (isset($user->id)) &bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull; @endif" />
									  <span class="input-group-btn">
											<a class="btn btn-blank" tabindex="-1" role="button" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="<div class='alert alert-sm alert-info alert-popover' role='alert'><p style='text-align: left;'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span> The new password must:</p><ul><li>Be at least 8 characters in length.</li><li>Contain at least 1 number.</li><li>Contain at least 1 capital letter.</li><li>Contain at least 1 lowercase letter.</li></ul></div>" data-html="true">
												<span class="fui-question-circle"></span>
											</a>
									  </span>
								   </div>
							   </div>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="form-group maxlength-no-button">
								<label class="control-label col-sm-3">Repeat Password</label>
								<div class="col-sm-9">
									<input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" value="" maxlength="125" placeholder="@if (isset($user->id)) &bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull; @endif" />
								</div>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="form-group maxlength-no-button">
								<label class="control-label col-sm-3">Forename</label>
								<div class="col-sm-9">
									<input type="text" name="forename" id="forename" class="form-control input-sm" value="{{ $user->forename or '' }}" maxlength="125" />
								</div>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="form-group maxlength-no-button">
								<label class="control-label col-sm-3">Surname</label>
								<div class="col-sm-9">
									<input type="text" name="surname" id="surname" class="form-control input-sm" value="{{ $user->surname or '' }}" maxlength="125" />
								</div>
							</div>
						</div>
						<hr/>
						<div class="row">
							<label class="control-label col-sm-3">Role</label>
							<div class="col-sm-9">
								<select name="role" id="role" class="form-control select select-primary select-sm @if (isset($user->id) && $user->id == Auth::user()->id) keep-disabled @endif" @if (isset($user->id) && $user->id == Auth::user()->id) disabled @endif>
									@foreach ($roles as $role)
										@if (isset($user->id))
											<option value="{{ $role->label }}" @if ($role->id == $user->role_id) selected @endif>{{ $role->label }}</option>
										@else
											<option value="{{ $role->label }}" @if ($role->label == 'Staff') selected @endif>{{ $role->label }}</option>
										@endif
									@endforeach
								</select>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="form-group maxlength-no-button">
								<label class="control-label col-sm-3">Email</label>
								<div class="col-sm-9">
									<input type="email" name="email" id="email" class="form-control input-sm" value="{{ $user->email or '' }}" maxlength="200" />
								</div>
							</div>
						</div>
						@if (config('app.sso_firefly_enabled') || config('app.sso_realsmart_enabled'))
							<hr/>
							<div class="row">
								<div class="form-group maxlength-no-button">
									<label class="control-label col-sm-3">SSO Allowed</label>
									<div class="col-sm-9">
										<label class="checkbox" for="sso">
											<input type="checkbox" name="sso" id="sso" value="1" data-toggle="checkbox" @if (isset($user->allow_sso) && $user->allow_sso) checked @endif />
										</label>
									</div>
								</div>
							</div>
						@endif
					</div>
				</div>
			</form>

			<div class="modal-footer">
				@if (isset($user->id))
					<form id="delete-user-form" action="{{ url('/modal/users', $user->id) }}" method="POST">
						{{ method_field('DELETE') }}
						<button type="submit" class="btn btn-sm btn-danger @if ($user->id == Auth::user()->id) keep-disabled @endif" @if ($user->id == Auth::user()->id) disabled @endif>
							<span class="glyphicon glyphicon-trash"></span> Delete User
						</button>
					</form>
				@endif
				<button type="button" id="add-edit-user-submit" class="btn btn-sm btn-info">
					@if (isset($user->id))
						<span class="fui-check"></span> Save Changes
					@else
						<span class="fui-plus"></span> Create User
					@endif
				</button>
			</div>
		</div>
	</div>
</div>

<script>
	@minify('js')

	$('.modal #add-edit-user-submit').click(function() {
		$('.modal #add-edit-user-form').submit();
	});

	$('.modal #add-edit-user-form').submit(function(e) {
		$('.modal #add-edit-user-form #role').prop('disabled', false);
		app.submitModalForm(this, {
			submitRef: '.modal #add-edit-user-submit',
			buttonSuccessText: '@if (isset($user->id)) Changes Saved @else User Created @endif'
		});
		e.preventDefault();
	});

	$('.modal #delete-user-form').submit(function(e) {
		app.submitModalForm(this, {
			confirm: 'Are you sure you want to delete this user?',
			buttonSuccessText: 'User Deleted'
		});
		e.preventDefault();
	});

	@endminify
</script>