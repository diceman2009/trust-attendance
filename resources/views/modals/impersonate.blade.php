{{-- Impersonate Modal --}}
<div id="modal-outer" class="modal fade" tabindex="-1" role="dialog" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h5 class="modal-title">Impersonate User</h5>
			</div>

			<form id="impersonate-form" action="{{ url('/modal/impersonate') }}" method="POST">
				<div class="modal-body">
					<div class="innerarea-form-modal">
						<div class="alert alert-sm alert-info" role="alert">
							<p>
								<span class="fui-info-circle" aria-hidden="true"></span> Impersonating a user allows you to view the site from their perspective. Any actions you take will be performed as if they had done them.
							</p>
							<p style="margin-top: 12px; font-style: italic;">Please note: Your booking date range will remain the same while impersonating another user.</p>
						</div>
						<div class="row">
							<div class="form-group maxlength-no-button">
								<label class="control-label col-sm-3">Select User</label>
								<div class="col-sm-9">
									<select id="user" name="user_id" class="form-control select select-primary select-sm">
										@foreach ($users as $user)
											<option value="{{ $user->id }}" @if (Auth::user()->id == $user->id)) selected @endif>
												{{ $user->forename }} {{ $user->surname }}  ({{ $user->username }})
											</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-info">
						<span class="fui-eye"></span> Impersonate
					</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	@minify('js')

	$('.modal #impersonate-form').submit(function(e) {
		if ('{{ count($users) > 0 }}') {
			app.submitModalForm(this, {
				successResponse: function(ret) {
					$('#modal-outer').on('hidden.bs.modal', function (e) {
						document.location.href = "{{ url('/') }}";
					});
				},
				success: function() {
					spinner.showLoader();
				}
			});
		}
		e.preventDefault();
	});

	@endminify
</script>