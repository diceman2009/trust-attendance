<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <!-- NAME: POP-UP -->
        <!--[if gte mso 15]>
        <xml>
        <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ $subject }}</title>
        
    <style type="text/css">
        p{
            margin:10px 0;
            padding:0;
        }
        table{
            border-collapse:collapse;
            font-family:Tahoma, Verdana, Segoe, sans-serif !important;
            font-size:12px !important;
        }
        h1,h2,h3,h4,h5,h6{
            display:block;
            margin:0;
            padding:0;
        }
        img,a img{
            border:0;
            height:auto;
            outline:none;
            text-decoration:none;
        }
        body,#bodyTable,#bodyCell{
            height:100%;
            margin:0;
            padding:0;
            width:100%;
            font-family:Tahoma, Verdana, Segoe, sans-serif;
            font-size:12px;
        }
        #outlook a{
            padding:0;
        }
        img{
            -ms-interpolation-mode:bicubic;
        }
        table{
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
        }
        .ReadMsgBody{
            width:100%;
        }
        .ExternalClass{
            width:100%;
        }
        p,a,li,td,blockquote{
            mso-line-height-rule:exactly;
        }
        a[href^=tel],a[href^=sms]{
            color:inherit;
            cursor:default;
            text-decoration:none;
        }
        p,a,li,td,body,table,blockquote{
            -ms-text-size-adjust:100%;
            -webkit-text-size-adjust:100%;
        }
        .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
            line-height:100%;
        }
        a[x-apple-data-detectors]{
            color:inherit !important;
            text-decoration:none !important;
            font-size:inherit !important;
            font-family:inherit !important;
            font-weight:inherit !important;
            line-height:inherit !important;
        }
        a.mcnButton{
            display:block;
        }
        .mcnImage{
            vertical-align:bottom;
        }
        .mcnTextContent{
            word-break:break-word;
        }
        .mcnTextContent img{
            height:auto !important;
        }
        .mcnDividerBlock{
            table-layout:fixed !important;
        }
        body,#bodyTable{
            background-color:#f2f2f2;
        }
        #bodyCell{
            border-top:0;
        }
        h1{
            color:#202020 !important;
            font-family:Tahoma, Verdana, Segoe, sans-serif;
            font-size:16px;
            font-style:normal;
            font-weight:normal;
            line-height:200%;
            letter-spacing:normal;
            text-align:center;
        }
        h2{
            color:#FFFFFF !important;
            font-family:Helvetica;
            font-size:26px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:left;
        }
        h3{
            color:#404040 !important;
            font-family:Helvetica;
            font-size:18px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:left;
        }
        h4{
            color:#606060 !important;
            font-family:Helvetica;
            font-size:16px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:left;
        }
        #templatePreheader{
            background-color:#34495e;
            border-top:0;
            border-bottom:0;
        }
        #preheaderBackground{
            background-color:#34495e;
            border-top:0;
            border-bottom:0;
        }
        .preheaderContainer .mcnTextContent,.preheaderContainer .mcnTextContent p{
            color:#FFFFFF;
            font-family:Tahoma, Verdana, Segoe, sans-serif;
            font-size:10px;
            line-height:125%;
            text-align:left;
        }
        .preheaderContainer .mcnTextContent a{
            color:#9ca4b4;
            font-weight:normal;
            text-decoration:none;
        }
        #templateHeader{
            background-color:#34495e;
            border-top:0;
            border-bottom:0;
        }
        #headerBackground{
            background-color:#FFFFFF;
            border-top:0;
            border-bottom:0;
        }
        .headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
            color:#202020;
            font-family:Helvetica;
            font-size:16px;
            line-height:150%;
            text-align:left;
        }
        .headerContainer .mcnTextContent a{
            color:#EE4343;
            font-weight:normal;
            text-decoration:underline;
        }
        #templateBody{
            background-color:#f2f2f2;
            border-top:0;
            border-bottom:0;
        }
        #bodyBackground{
            background-color:#FFFFFF;
            border-top:0;
            border-bottom:0;
        }
        .bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
            color:#202020;
            font-family:Tahoma, Verdana, Segoe, sans-serif;
            font-size:12px;
            line-height:200%;
            text-align:left;
        }
        .bodyContainer .mcnTextContent a{
            color:#EE4343;
            font-weight:normal;
            text-decoration:underline;
        }
        #templateFooter{
            background-color:#f2f2f2;
            border-top:0;
            border-bottom:0;
        }
        #footerBackground{
            background-color:#f2f2f2;
            border-top:0;
            border-bottom:0;
        }
        .footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
            color:#797979;
            font-family:Helvetica;
            font-size:10px;
            line-height:125%;
            text-align:center;
        }
        .footerContainer .mcnTextContent a{
            color:#606060;
            font-weight:normal;
            text-decoration:underline;
        }
        @media only screen and (max-width: 480px){
            body,table,td,p,a,li,blockquote{
                -webkit-text-size-adjust:none !important;
            }

        }   @media only screen and (max-width: 480px){
                body{
                    width:100% !important;
                    min-width:100% !important;
                }

        }   @media only screen and (max-width: 480px){
                .templateContainer{
                    max-width:600px !important;
                    width:100% !important;
                }

        }   @media only screen and (max-width: 480px){
                .mcnImage{

                }

        }   @media only screen and (max-width: 480px){
                .mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
                    max-width:100% !important;
                    width:100% !important;
                }

        }   @media only screen and (max-width: 480px){
                .mcnBoxedTextContentContainer{
                    min-width:100% !important;
                }

        }   @media only screen and (max-width: 480px){
                .mcnImageGroupContent{
                    padding:9px !important;
                }

        }   @media only screen and (max-width: 480px){
                .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
                    padding-top:9px !important;
                }

        }   @media only screen and (max-width: 480px){
                .mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
                    padding-top:18px !important;
                }

        }   @media only screen and (max-width: 480px){
                .mcnImageCardBottomImageContent{
                    padding-bottom:9px !important;
                }

        }   @media only screen and (max-width: 480px){
                .mcnImageGroupBlockInner{
                    padding-top:0 !important;
                    padding-bottom:0 !important;
                }

        }   @media only screen and (max-width: 480px){
                .mcnImageGroupBlockOuter{
                    padding-top:9px !important;
                    padding-bottom:9px !important;
                }

        }   @media only screen and (max-width: 480px){
                .mcnTextContent,.mcnBoxedTextContentColumn{
                    padding-right:18px !important;
                    padding-left:18px !important;
                }

        }   @media only screen and (max-width: 480px){
                .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
                    padding-right:18px !important;
                    padding-bottom:0 !important;
                    padding-left:18px !important;
                }

        }   @media only screen and (max-width: 480px){
                .mcpreview-image-uploader{
                    display:none !important;
                    width:100% !important;
                }

        }</style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell" style="padding-bottom:40px;">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN PREHEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader">
                                        <tr>
                                            <td align="center" valign="top" style="padding-right:10px; padding-left:10px;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="preheaderBackground">
                                                                <tr>
                                                                    <td valign="top" class="preheaderContainer">
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
                                                                            <tbody class="mcnImageBlockOuter">
                                                                                <tr>
                                                                                    <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                                                                                        <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; height: 31px; padding-top: 10px; padding-bottom: 0; color: #FFF; font-size: 16.5px; ">
                                                                                                        <img align="left" alt="" src="{{ url('img/rb-navbar.png') }}" width="23" style="max-width:35px; padding-bottom: 0; margin-right: 6px; display: inline !important;" class="mcnImage"> <b>Resource Booking</b>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END PREHEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                        <tr>
                                            <td align="center" valign="top" style="padding-right:10px; padding-left:10px;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="headerBackground">
                                                                <tr>
                                                                    <td valign="top" class="headerContainer">
                                                                        @if (File::exists(public_path('/custom/logo.jpg')))
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
                                                                                <tbody class="mcnDividerBlockOuter">
                                                                                    <tr>
                                                                                        <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 6px 18px;">
                                                                                            <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <span></span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
                                                                                <tbody class="mcnImageBlockOuter">
                                                                                        <tr>
                                                                                            <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                                                                                                <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">
                                                                                                                <img align="center" alt="" src="{{ url('/custom/logo.jpg') }}" height="120" style="max-height:125px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                </tbody>
                                                                            </table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
                                                                                <tbody class="mcnDividerBlockOuter">
                                                                                    <tr>
                                                                                        <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 12px 18px 0px;">
                                                                                            <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <span></span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td align="center" valign="top" style="padding-right:10px; padding-left:10px;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyBackground">
                                                                <tr>
                                                                    <td valign="top" class="bodyContainer">
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
                                                                            <tbody class="mcnDividerBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 14px 18px;">
                                                                                        <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <span></span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                                                                            <tbody class="mcnTextBlockOuter">
                                                                                <tr>
                                                                                    <td valign="top" class="mcnTextBlockInner">
                                                                                        
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                                                                                            <tbody><tr>
                                                                                                
                                                                                                <td valign="top" class="mcnTextContent" style="padding: 9px 18px; font-family: Tahoma, Verdana, Segoe, sans-serif; font-size: 16px; line-height: 200%; text-align: left;">
                                                                                                
                                                                                                    <h1 class="null" style="text-align: left;"><span style="line-height:20.8px">{{ $subject }}</span></h1>

                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody></table>
                                                                                        
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                                                                            <tbody class="mcnTextBlockOuter">
                                                                                <tr>
                                                                                    <td valign="top" class="mcnTextBlockInner">
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                                                                                        <div>{!! $content !!}</div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
                                                                            <tbody class="mcnDividerBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 14px 18px;">
                                                                                        <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <span></span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
                                        <tr>
                                            <td align="center" valign="top" style="padding-right:10px; padding-left:10px;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="footerBackground">
                                                                <tr>
                                                                    <td valign="top" class="footerContainer"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
                                                                        <tbody class="mcnDividerBlockOuter">
                                                                            <tr>
                                                                                <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 10px 18px;">
                                                                                    <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: none;border-top-color: #EAEAEA;">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <span></span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                                                                        <tbody class="mcnTextBlockOuter">
                                                                            <tr>
                                                                                <td valign="top" class="mcnTextBlockInner">
                                                                                    
                                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                                                                                        <tbody>
                                                                                            <tr>  
                                                                                                <td valign="top" class="mcnTextContent" style="padding: 9px 18px; font-family: Tahoma, Verdana, Segoe, sans-serif; font-size: 10px; font-style: normal; line-height: 150%; text-align: center;">
                                                                                                
                                                                                                    This is an automated email.&nbsp;If you have received this in error or wish to unsubscribe from these emails please contact <a href="mailto:{{ config('mail.support_address') }}" target="_blank">{{ config('mail.support_address') }}</a>.
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>