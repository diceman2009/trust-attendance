var spinner = {};

spinner.version = 1.01;
spinner.html = '<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>';
spinner.baseLocation = (typeof navbar != 'undefined' && navbar.baseURL != '') ? navbar.baseURL : '';
spinner.legacyHTML = '<div style="width:100%; text-align:center;"><img src="' + spinner.baseLocation + '/img/loader.gif" /></div>';


//SHOW THE LOADER
spinner.showLoader = function(delaytime) {
	if ($('#page-loader').length == 0) {
		$('body').append('<div id="page-loader"></div>');
	}
	if (!$('#page-loader').is(':visible')) {
		$('#page-loader').css('opacity', 0);
		$('#page-loader').html((spinner.supportsTransitions()) ? spinner.html : spinner.legacyHTML);
		$('#page-loader').show();
		$('#page-loader').transition({ opacity: 1, delay: delaytime, duration: 180 });
	}
}

//HIDE THE LOADER
spinner.hideLoader = function(contFunc) {
	if (typeof sys != 'undefined') { sys.resize(); }
	if ($('#page-loader').is(':visible')) {
		$('#page-loader').transition({ opacity: 0, complete: function() { 
			$('#page-loader').hide().html('');
			if (contFunc) { contFunc(); }
		}
	});
	} else {
		$('#page-loader').stop();
		$('#page-loader').hide().html('');
		if (contFunc) { contFunc(); }
	}
}

//POSITION LOADER
spinner.positionLoader = function(header, footer) {
	if ($('#page-loader').length == 0) {
		$('body').append('<div id="page-loader"></div>');
	}
	header = (!header) ? 0 : header;
	footer = (!footer) ? 0 : footer;
	$('#page-loader').css({ 'padding-top': '30px', 'top': header + 'px', 'bottom': footer + 'px' });
}


//BUTTON LOADERS

//SETUP AND START
spinner.startButtonLoader = function(obj) {
	if (!obj.successText) {	obj.successText = 'Success'; }
	if (!obj.errorText) { obj.errorText = 'Error'; }
	if (obj.resetAfterError == null) { obj.resetAfterError = true; }
	obj.hasOriginalSuccessClass = obj.element.hasClass('btn-success');
	obj.hasOriginalDangerClass = obj.element.hasClass('btn-danger');
	obj.savedText = obj.element.html();
	obj.savedOnClick = obj.element.attr('onClick');
	obj.element.removeAttr('onClick');
	if (!obj.element.hasClass('btn-block')) {
		obj.element.css('width', obj.element.outerWidth());
	}
	obj.element.addClass('no-pointer');
	obj.element.html((spinner.supportsTransitions()) ? spinner.html : 'Loading...');
	obj.timeout = setTimeout(function(){
		spinner.completeButtonLoader(obj, 'timeout');
	}, 400);
	return obj;
}

//ON COMPLETION OF DATA REQUEST + INITIAL TIMEOUT
spinner.completeButtonLoader = function(obj, from) {
	if (from == 'timeout') { obj.timeout = null; } else { obj.requestComplete = from; }
	if (obj.timeout == null) {
		if (obj.requestComplete === true) {
			spinner.afterButtonLoaderComplete(obj, 'success');
		} else if (obj.requestComplete === false) {
			spinner.afterButtonLoaderComplete(obj, 'error');
		} else if (obj.requestComplete == 'reset') {
			spinner.afterButtonLoaderComplete(obj, 'reset');
		} else if (obj.requestComplete == 'success-reset') {
			spinner.afterButtonLoaderComplete(obj, 'success-reset');
		}
	}
}

//SETUP AFTER COMPLETE MESSAGE + TIMEOUT
spinner.afterButtonLoaderComplete = function(obj, outcome) {
	if (outcome == 'success') {
		obj.element.html(obj.successText).removeClass('btn-danger').addClass('btn-success');
		setTimeout(function(){ 
			if (obj.resetAfterComplete) { spinner.resetButtonLoader(obj); }
			obj.success(); 
		}, 1300);
	} else if (outcome == 'error') {
		obj.element.html(obj.errorText).removeClass('btn-success').addClass('btn-danger');
		setTimeout(function(){ 
			if (obj.resetAfterError) {	spinner.resetButtonLoader(obj); }
			obj.error(); 
		}, 1300);
	} else if (outcome == 'reset') {
		spinner.resetButtonLoader(obj);
		obj.error();
	} else if (outcome == 'success-reset') {
		spinner.resetButtonLoader(obj);
		obj.success(); 
	}
}

//SETS BUTTON BACK TO ITS DEFAULT STATE AFTERWARDS
spinner.resetButtonLoader = function(obj) {
	obj.element.html(obj.savedText);
	obj.element.css('width', '').removeClass('btn-success btn-danger no-pointer');
	if (obj.hasOriginalSuccessClass) { obj.element.addClass('btn-success'); }
	if (obj.hasOriginalDangerClass) { obj.element.addClass('btn-danger'); }
	obj.element.attr('onClick', obj.savedOnClick);
}

//DOES BROWSER SUPPORT CSS TRANSITIONS
spinner.supportsTransitions = function() {
	var b = document.body || document.documentElement, s = b.style, p = 'transition';
	if (typeof s[p] == 'string') { return true; }
	var v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
	p = p.charAt(0).toUpperCase() + p.substr(1);
	for (var i=0; i<v.length; i++) {
		if (typeof s[v[i] + p] == 'string') { return true; }
	}
	return false;
}