var util = {};

util.version = 1.11;

//SAVE DATA TO A COOKIE
util.saveInCookie = function(cname, cvalue, exdays) {
	if(!exdays){
		exdays = 30;
	}
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = 'expires='+d.toGMTString();
	document.cookie = cname + '=' + cvalue + ';path=/; ' + expires;
}

//LOAD DATA FROM A COOKIE
util.loadFromCookie = function(cname) {
	var name = cname + '=';
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1);
		if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
	}
	return '';
}

//DELETE A COOKIE
util.deleteCookie = function(cname) {
	document.cookie = cname + '=;path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

//SAVE IN LOCALSTORAGE
util.saveInStorage = function(name, value) {
	if (typeof(localStorage) !== 'undefined') {
		try {
			localStorage.setItem(name, value);
		} catch(e) {}
	}
}

//LOAD FROM LOCALSTORAGE
util.loadFromStorage = function(name) {
	if (typeof(localStorage) !== 'undefined') {
		try {
			var loaded = localStorage.getItem(name);
			if (loaded !== null) {
				return loaded;
			}
		} catch(e) {}
	}
	return '';
}

//DELETE FROM LOCALSTORAGE
util.deleteFromStorage = function(name) {
	if (typeof(localStorage) !== 'undefined') {
		try {
			localStorage.removeItem(name);
		} catch(e) {}
	}
}

//SAVE IN SESSIONSTORAGE
util.saveInSessionStorage = function(name, value) {
	if (typeof(sessionStorage) !== 'undefined') {
		try {
			sessionStorage.setItem(name, value);
		} catch(e) {}
	}
}

//LOAD FROM SESSIONSTORAGE
util.loadFromSessionStorage = function(name) {
	if (typeof(sessionStorage) !== 'undefined') {
		try {
			var loaded = sessionStorage.getItem(name);
			if (loaded !== null) {
				return loaded;
			}
		} catch(e) {}
	}
	return '';
}

/// FIX FOR IE NOT HAVING INDEXOF FUNCTION
if (!Array.prototype.indexOf) { 
	Array.prototype.indexOf = function(obj, start) {
		 for (var i = (start || 0), j = this.length; i < j; i++) {
			 if (this[i] === obj) { return i; }
		 }
		 return -1;
	}
}

//READS OBJECT GIVEN IN ARRAY FORM LIKE [obj, property]
util.readObj = function(arr) {
	return arr[0][arr[1]];
}

/// PREFERENCES (QUERY STRING PARAMS)
util.setPreferenceValues = function() {
	util.preferenceNamesArray = [];
	util.preferenceValuesArray = [];
	var url = document.location.href.split('#', 1)[0];
	var queryStrings = url.split('?');
	if (queryStrings.length > 1) {
		var queryArray = queryStrings[1].split('&');
		for (var x = 0; x < queryArray.length; x++) {
			var valueArray = queryArray[x].split('=');
			util.preferenceNamesArray.push(valueArray[0]);
			util.preferenceValuesArray.push(valueArray[1]);
		}
	}
}
util.setPreferenceValues(); 

/// GET A QUERYSTRING VALUE (AKA PREFERENCE)
util.getPreferenceValue = function(value) {
	var loc = util.preferenceNamesArray.indexOf(value);
	if (loc > -1) {
		return util.preferenceValuesArray[loc]; 
	}
	return '';  
}

//SEARCH AN ARRAY USING OPTIONAL PROPERTY NAMES
util.getIndexByName = function(a, s, p1, p2, p3) {
	if (!a) { return -1; }
	p1 = typeof p1 !== 'undefined' ? p1 : '';
	p2 = typeof p2 !== 'undefined' ? p2 : '';
	p3 = typeof p3 !== 'undefined' ? p3 : '';
	for (var i = 0; i < a.length; i++) {
		if (p1 != '' && p2 != '' && p3 != '') {
			if (a[i][p1][p2][p3] == s) { return i; }
		} else if (p1 != '' && p2 != '') {
			if (a[i][p1][p2] == s) { return i; }
		} else if (p1 != '') {
			if (a[i][p1] == s) { return i; }
		} else {
			if (a[i] == s) { return i; }
		}
	}
	return -1;
}

//CHECK IF STRING IS IN JSON FORMAT
util.isJSON = function(s) {
	try { JSON.parse(s); } catch (e) { return false; }
	return true;
}

//CLONE AN OBJECT
util.cloneArray = function(arr) {
	if (arr == null) { return []; }
	return JSON.parse(JSON.stringify(arr));
}

//CLONE AN OBJECT
util.cloneObject = function(obj) {
	return jQuery.extend(true, {}, obj);
}

//REPLACE ALL OCCURANCES OF A STRING INSIDE ANOTHER STRING
util.replaceInString = function(find, replace, str) {
	return str.replace(new RegExp(find, 'g'), replace);
}

//MAKES RANDOM NUMBER BETWEEN TWO VALUES
util.makeRandomNumber = function(low, high) {
	return Math.floor((Math.random() * high) + low);
}

//MAKE A NUMBER 2 DIGITS LONG
util.makeTwoDigits = function(s) {
	var num = parseInt(s);
	if (num < 10) { return '0' + num; }
	return s;
}

//ONLY ALLOW NUMERIC CHARACTERS
jQuery.fn.forceNumeric = function () {
	return this.each(function () {
		$(this).keydown(function (e) {
			var key = e.which || e.keyCode;
			if ((!e.shiftKey && !e.altKey && !e.ctrlKey &&
				key >= 48 && key <= 57 || //numbers
				key >= 96 && key <= 105 || //Numeric keypad
				key == 190 || key == 110 || //comma, period and minus, . on keypad
				key == 8 || key == 9 || key == 13 || //Backspace and Tab and Enter
				key == 35 || key == 36 || //Home and End
				key == 37 || key == 39 || //left and right arrows
				key == 46 || key == 45) || //Del and Ins
				String.fromCharCode(key) == ';') //KEY is 
				return true;
			return false;
		});
	});
}

//GET BROWSER VERSION
util.getBrowser = function() {
	var ua= navigator.userAgent, tem, 
	M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
	if(/trident/i.test(M[1])){
		tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
		return 'IE '+(tem[1] || '');
	}
	if(M[1]=== 'Chrome'){
		tem= ua.match(/\bOPR\/(\d+)/)
		if(tem!= null) return 'Opera '+tem[1];
	}
	M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
	if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
	return M;
}

//ESCAPE HTML
util.entityMap = { '&': '&amp;', '<': '&lt;', '>': '&gt;', '"': '&quot;', "'": '&#39;', '/': '&#x2F;' };
util.escapeHTML = function(str) {
	return String(str).replace(/[&<>"'\/]/g, function (s) {
		return util.entityMap[s];
	});
}

//UNESCAPE HTML
util.entityMapReverse = { '&amp;': '&', '&lt;': '<', '&gt;': '>', '&quot;': '"', '&#39;': "'", '&#x2F;': '/' };
util.unescapeHTML = function(str) {
	return String(str).replace(/&amp;|&lt;|&gt;|&quot;|&#39;|&#x2F;/g, function (s) {
		return util.entityMapReverse[s];
	});
}

//REPLACE CARRIAGE RETURNS
util.replaceCarriageReturns = function(str, html) {
	if (str == null) { return ''; }
	if (html === true) {
		return String(str).replace(/\n/g, '<br/>');
	} else {
		return String(str).replace(/<br\/>/g, '\n');
	}
}

//GET TODAYS DATE
util.makeTodaysDate = function() {
	var d = new Date();
	return new Date(d.getFullYear(), d.getMonth(), d.getDate());
}

//CONVERT TO DATE FROM STRING
util.convertToDate = function(str) { //str should be dd/mm/yy
	var split = str.split('/');
	return new Date('20' + split[2], split[1]-1, split[0]);	
}

util.convertToDateB = function(str) { //str should be yyyy-mm-dd
	var split = str.split('-')
	return new Date(split[0], split[1]-1, split[2]);	
}

//MAKE DATESTRING FROM JS DATE
util.toDateString = function(date) {
	return date.getFullYear() + '-' + util.addLeadingZero(date.getMonth() + 1) + '-' + util.addLeadingZero(date.getDate());
}

//CUT YEAR OFF DATE
util.shortenDate = function(date) {
	if (date == '01/01/70') { return '00/00'; }
	var dateSplit = date.split('/');
	return dateSplit[0] + '/' + dateSplit[1];
}

//ADD LEADING ZERO TO NUMBER BELOW 10 (good with dates!)
util.addLeadingZero = function(num) {
	if (num < 10) {
		return '0' + num;
	}
	return num;
}

//MAKE ARRAY WITH A PROPERTY FROM AN ARRAY OF OBJECTS
util.makeArray = function(arr, property1, property2) {
	var newArr = [];
	for (var x = 0; x < arr.length; x++) {
		var str = arr[x][property1];
		if (property2) { str += ' ' + arr[x][property2]; }
		newArr.push(str);
	}
	return newArr;
}

//COPYS AN ARRAY, CHECKS FOR AN ITEM, THEN ADDS THAT ITEM IF NOT FOUND
util.copyArrayAndAddItem = function(arr, id, value, newitem, addtoend) {
	var tempArr = util.cloneArray(arr);
	var loc = util.getIndexByName(arr, value, id);
	if (loc == -1) {
		if (newitem[id] != null) {
			if (addtoend) { 
				tempArr.push(newitem); 
				loc = tempArr.length - 1; 
			} else { 
				tempArr.unshift(newitem); 
			}
		}
		if (loc == -1) { loc = 0; }
	}
	return { 'arr': tempArr, 'loc': loc };
}

//COPYS A SETTINGS ARRAY AND ALSO COPIES THE NESTED DATA ARRAY TO THE TOP LEVEL
util.copyAndFlattenSettingsArray = function(arr) {
	var tempArr = util.cloneArray(arr);
	for (var x = 0; x < tempArr.length; x++) {
		for (var property in tempArr[x]['data']) {
			if (tempArr[x]['data'].hasOwnProperty(property)) {
				tempArr[x]['data.' + property] = tempArr[x]['data'][property];
			}
		}
	}
	return tempArr;
}

//GET CURRENT PERIOD OF THE DAY (BASED ON CLIENT TIME)
util.getPeriod = function() {
	if (sys.periods == null) { return ''; }
	var d = new Date();
	var now = (d.getHours() * 100) + d.getMinutes();
	for(var x = 0; x < sys.periods.length; x++){
		if (now <= sys.periods[x]['data']['end'] && now >= sys.periods[x]['data']['start']) {
			return sys.periods[x].id;
		}
	}
	if (sys.periods.length > 0) {	return sys.periods[0].id; }
	return '';
}

//CONVERT THE DETENTION TITLE TO A PREFIX FOR C1, C2, C3 etc
util.convertDetentionTitleToPrefix = function(title) {
	var str = title.substr(title.length - 1, 1);
	if (!isNaN(str)) {
		str = title.substr(0, title.length - 1);
	} else {
		str = 'Reason ';
	}
	return str;
}

//SELECT TEXT FIELD FROM THE BUTTON CLICK
util.selectDatePickerFromButton = function(ref) {
	if (!bowser.ios && !bowser.android) {
		var dateField = $(ref);
		var len = dateField.val().length * 2;
		dateField.focus();
		if (typeof dateField[0].setSelectionRange == 'function') {
			dateField[0].setSelectionRange(len, len);
		}
	}
}

//CONFIRM WITH SUPPORT FOR DISABLED ALERTS
util.confirm = function(msg) {
	var beforeTime = Math.floor(new Date().getTime());
	var conf = confirm(msg);
	var afterTime = Math.floor(new Date().getTime());
	if (afterTime - beforeTime > 50) { //using a time difference fixes issue if alerts are disabled by the user
		if (!conf) { 
			return false; 
		}
	}
	return true;
}

//GENERATE A RANDOM GUID
util.generateGUID = function() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

$(document).ready(function() {
	//FIX FOR IE NOT HAVING INDEXOF FUNCTION
	if (!Array.prototype.indexOf) { 
		Array.prototype.indexOf = function(obj, start) {
			for (var i = (start || 0), j = this.length; i < j; i++) {
				if (this[i] === obj) { return i; }
			}
			return -1;
		}
	}

	//ADDS TRIM TO IE STRINGS
	if (typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, ''); 
		}
	}
});