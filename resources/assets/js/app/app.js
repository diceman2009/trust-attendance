var app = {};

app.version = 1.21;

//PERFORM OUR STARTUP TASKS
$(document).ready(function() {
	$('#main-content').transition({ opacity: 1, duration: 600, queue:false });
	spinner.positionLoader($('.navbar-fixed-top').height());
	app.addHistoryItem();
	app.setupNavbar();
	app.pageStartup();
	app.scrollListen();
	if (bowser.ios) { $('html').addClass('ios'); }
});

//SETUP OUR MAIN NAVBAR
app.setupNavbar = function() {
	$('#navbar-main ul.navbar-nav li').each(function() {
		if ($(this).data('urlpath')) {
			var linkType = $(this).data('linktype');
			$(this).find('a').click(function(e) {
				var url = $(this).parent().data('urlpath');
				e.preventDefault();
				if (linkType == 'modal') {
					app.loadModalHTML(url, true);
				} else {
					app.loadPageHTML(url, true);
				}
			});
		}
	});
	$('.navbar .navbar-header a.navbar-brand').click(function(e) {
		e.preventDefault();
		app.loadPageHTML($(this).prop('href'), true);
	});
	$('.navbar .collapse').on('show.bs.collapse', function(e) {
		$('.navbar-fixed-top .navbar-collapse').addClass('in');
		e.preventDefault();
	});
	$('.navbar .collapse').on('hide.bs.collapse', function(e) {
		$('.navbar-fixed-top .navbar-collapse').removeClass('in');
		e.preventDefault();
	});
	app.setActiveNavbarItem();
}

//CHANGES THE ACTIVE NAVBAR ITEM BASED ON THE PROVIDED URL
app.setActiveNavbarItem = function(url) {
	if (url == null) {
		url = document.location.href;
	}
	url = url.split('?')[0];
	url = url.split('#')[0];

	$('.navbar-nav li').removeClass('active');
	var item = $('#navbar-main').find('[data-urlpath="' + url + '"]');
	if (item.length > 0) {
		if (!$(item).hasClass('avoid-active')) {
			$(item).addClass('active');
		}
	} else if (typeof navbar != 'undefined') {
		if (navbar.baseURL) {
			url = url.replace(navbar.baseURL, '');
			segments = url.split('/');
			if (segments.length >= 2) {
				url = navbar.baseURL + '/' + segments[1];
			} else {
				url = navbar.baseURL;
			}
		}
		var item = $('#navbar-main').find('[data-urlpath="' + url + '"]');
		if (item.length > 0) {
			if (!$(item).hasClass('avoid-active')) {
				$(item).addClass('active');
			}
		}
	}
}

//ADDS ITEM TO OUR HISTORY (if supported by browser)
app.addHistoryItem = function(url) {
	if (history.pushState) {
		if ('scrollRestoration' in history) {
			history.scrollRestoration = 'manual';
		}
		var newHistoryID = 'history_scroll_' + util.makeRandomNumber(1, 999999999999);
		if (!url) { //required to add initial back button support
			window.history.replaceState({ 'path': window.location.href, 'id': newHistoryID }, '', window.location.href);
		} else {
			if (url == app.historyPath) {
				return;
			}
			app.historyPath = url;
			if (!app.historyID) {
				app.historyID = 'history_scroll_' + util.makeRandomNumber(1, 999999999999);
			}
			window.history.replaceState({ 'path': window.location.href, 'id': app.historyID }, '', window.location.href);
			window.history.pushState({ 'path': url, 'id': newHistoryID }, '', url);

		}
		app.historyID = newHistoryID;
		util.saveInSessionStorage(app.historyID, $(window).scrollTop());
	}
	app.updateGoogleAnalytics();
}

//ON BACK BUTTON PRESS
window.onpopstate = function(e) {
	if ($('body').hasClass('modal-open')) { 
		window.history.forward();
	 } else if (e.state) { 
		$('#select2-drop-mask').click(); //close any selects that might be open
		if (typeof(app.onPopState) === 'function') {
			app.onPopState(e);
		}
		if (app.scrollTimeout !== null) {
			clearTimeout(app.scrollTimeout);
		}
		app.historyID = e.state.id;
		app.historyPath = e.state.path;
		var scrollPos = util.loadFromSessionStorage(e.state.id);
		if (scrollPos == '') {
			scrollPos = 0;
		}
		app.updateGoogleAnalytics();
		app.loadPageHTML(e.state.path, false, true, scrollPos);
	}
}

//LISTEN FOR SCROLL STOPS TO UPDATE OUR BACK BUTTON SCROLL POSITION
app.scrollListen = function() {
	if (history.pushState) {
		$(window).scroll(function() {
			if (app.scrollTimeout !== null) {
				clearTimeout(app.scrollTimeout);        
			}
			var scrollPos = $(window).scrollTop();
			app.scrollTimeout = setTimeout(function() {
				if (app.historyID) {
					util.saveInSessionStorage(app.historyID, scrollPos);
				}
			}, 75);
		});
	}
}

//UPDATES GOOGLE ANALYTICS IF IT HAS BEEN LOADED
app.updateGoogleAnalytics = function() {
	if (typeof ga != 'undefined') {
		var currentPath = document.location.href;
		currentPath = currentPath.split('?')[0];
		currentPath = currentPath.split('#')[0];

		var analyticPath = '';
		var paths = currentPath.split('/');
		for (var x = 3; x < paths.length; x++) {
			analyticPath += paths[x];
			if (x < paths.length - 1) {
				analyticPath += '/';
			}
		}

		ga('set', 'page', analyticPath);
		ga('send', 'pageview');
	}
}

//ADDS WARNING BEFORE LEAVING PAGE (if required)
window.onbeforeunload = function(e) { 
	if ($('body').hasClass('modal-open') && app.modalCloseWarning) { 
		return 'This modal has unsaved changes, are you sure you want to leave?'; 
	} else if (typeof(app.customBeforeUnload) === 'function') {
		return app.customBeforeUnload();
	}
}

//LOADS SECTION OF AJAX
app.loadSectionHTML = function (opts) {
	if (typeof opts == 'undefined') {
		var opts = {
			url: null,				//required url to load via ajax
			section: null,			//required id of html section to update
			addHistoryURL: null,	//url to be added to the browser history
			showLoader: true,		//show the loader (default: true)
			scrollTo: null, 		//id to scroll to after updating html
			error: null,			//callback for error
			success: null			//callback for success
		};
	}

	if (opts.addHistoryURL) {
		setTimeout(function() { app.addHistoryItem(opts.addHistoryURL); }, 150);
	}
	if (opts.showLoader !== false) {
		spinner.showLoader();
	}

	data.performRequest({
		url: opts.url,
		method: 'GET',
		dataType: 'html',
		success: function(ret) {
			app.addHTML(ret, opts.section);
			app.pageStartup(opts.section);
			spinner.hideLoader();
			if (opts.scrollTo) {
				$(window).scrollTop($(opts.scrollTo).offset().top - $('.navbar-fixed-top').height());
			}
			if (opts.success) {
				opts.success();
			}
		},
		error: function(ret) {
			spinner.hideLoader();
			if (opts.error) {
				opts.error();
			}
		}
	}, false, 'page');
}

//LOADS PAGE VIA AJAX BUT DOESNT ANIMATE THE CONTENT
app.loadSamePageHTML = function (url, addHistory) {
	if (addHistory === null) {
		addHistory = true;
	}
	app.loadPageHTML(url, addHistory, null, null, true);
}

//LOAD PAGE VIA AJAX AND DISPLAY (will add history and changed the selected navbar item as appropriate)
app.loadPageHTML = function(url, addHistory, fromBackButton, scrollPos, dontHideContent, onErrorFunction) {
	app.setActiveNavbarItem(url);
	data.cancelAllRequests('page');
	if (addHistory) {
		setTimeout(function() { app.addHistoryItem(url); }, 150);
	}
	spinner.showLoader();
	if (!dontHideContent) {
		$('#main-content').css('opacity', 0);
	}

	data.performRequest({
		url: url,
		method: 'GET',
		dataType: 'html',
		success: function(ret) {
			if (dontHideContent) {
				app.addHTML(ret, '#main-content');
			} else {
				app.addHTML(ret, '#main-content', true);
			}
			if (fromBackButton) {
				$(window).scrollTop(scrollPos);
			} else {
				$(window).scrollTop(0);
			}
			app.pageStartup('#main-content');
			spinner.hideLoader();
		},
		error: function(ret) {
			if (onErrorFunction) {
				onErrorFunction(ret);
			} else {
				if (typeof(ret) == 'object') {
					if (ret.status == 404) {
						var html = $.parseHTML('<div>' + ret.responseText + '</div>');
						var content = $(html).find('#dashboard-content');
						if (content) {
							app.addHTML(content, '#main-content', true);
							$(window).scrollTop(0);
						}
					}
				}
				spinner.hideLoader();
			}
		}
	}, false, 'page');
}

//ON NEW PAGE STARTUP
app.pageStartup = function(enclosed) {
	$('#navbar-main ul.navbar-nav li a').blur();

	if (!enclosed) { enclosed = ''; }
	$(enclosed + ' a.pagelink').click(function() {
        app.loadPageHTML($(this).attr('href'), true);
        return false;
    });
    $(enclosed + ' button.pagelink').click(function() {
        app.loadPageHTML($(this).data('urlpath'), true);
    });

    $(enclosed + ' a.modallink').click(function(e) {
    	if ($(this).data('modalpath') != '') {
    		app.loadModalHTML($(this).data('modalpath'), true);
    	} else {
    		app.loadModalHTML($(this).attr('href'), true);
    	}
        e.preventDefault();
    });
    $(enclosed + ' button.modallink').click(function() {
        app.loadModalHTML($(this).data('urlpath'), true);
    });

    $(enclosed + ' .page-print-button').click(function() {
		window.print();
		e.preventDefault();
    });

	$(enclosed + ' img.display-pic').one('load', function() {
		$(this).transition({ opacity: 1 });
	}).each(function() {
		if (this.complete) {
			$(this).load();
		}
	});

	$(enclosed + ' .sortabletable').tablesorter({
		'sortList': [[0, 0]]
	});
	$(enclosed + ' .sortabletable-nosort').tablesorter();
	helpers.start();
}

//LOAD MODAL AND OPEN IT
app.loadModalHTML = function(url, initial, onSuccess, onError) {
	if (initial) {
		app.previousModalURL = '';
		spinner.showLoader();
	}
	data.cancelAllRequests('modal');

	app.loadingModal = true;

	data.performRequest({
		url: url,
		method: 'GET',
		dataType: 'html',
		success: function(ret, status) {
			app.loadingModal = false;
			app.modalCloseWarning = false;
			app.nextModalHTML = ret;
			if (app.modalHidden || initial) {
				app.addHTML(app.nextModalHTML, '#modal-holder', false);
				spinner.hideLoader();
				app.modalStartup();
				app.nextModalHTML = null;
				if (initial) {
					app.globalBackdrop('add');
				}
			}
			app.previousModalURL = url;
			if (onSuccess) {
				onSuccess();
			}
		},
		error: function(ret) {
			app.loadingModal = false;
			if (app.previousModalURL != '') {
				var prevURL = app.previousModalURL;
				app.previousModalURL = '';
				app.loadModalHTML(prevURL);
			} else {
				app.globalBackdrop('remove');
				spinner.hideLoader();
			}
			if (onError) {
				onError();
			}
		}
	}, false, 'modal');
}

//ADD HTML TO SCREEN
app.addHTML = function(html, replace, animate) {
	if (replace == null) {
		replace = '#main-content';
	} else if (replace == '#modal-holder') {
		if ($('#modal-holder').length == 0) {
			$('body').append('<div id="modal-holder"></div>')
		}
	}

	if (html) {
		$(replace).html(html);
	}

	if (animate) {
		$(replace).css('opacity', 0);
		$(replace).transition({ opacity: 1, duration: 600, queue:false });
	}
}

//REGULAR FORM SUBMIT
//adds optional button loader, prevents multiple submits, has optional success and error callbacks
app.submitForm = function(form, opts) {
	if (typeof opts == 'undefined') {
		var opts = {
			submitRef: null, 			//reference to submit button if outside of the form tags (default: null)
			beforeSubmitFunction: null,	//optional function to be executed before we submit form
			showLoader: false,			//optional full screen loader (default: false)
			buttonLoader: true,			//optional button loader (default: true)
			resetButtonLoader: false,   //optional reset the button loader instead of showing success/error text
			buttonSuccessText: null, 	//button text for when it completes successfully occurs (defaults to the spinner default)
			buttonErrorText: null,		//button text for when an error occurs (defaults to the spinner default)
			error: null,				//callback for error
			success: null,				//callback for success
			errorFromButton: null,		//callback for error (after button animation finished)
			successFromButton: null,	//callback for success (after button animation finished)
			disableElements: false,		//optional do we want to disable our form elements while the form submits
			confirm: null				//confirm dialog message (default: null)			
		};
	}

	//display confirm box
	if (opts.confirm) {
		if (!util.confirm(opts.confirm)) {
			return;
		}
	}
	
	if ($(form).data('submitting') === true) {
		return;
	}
	$(form).data('submitting', true);
	
	if (opts.beforeSubmitFunction) {
		opts.beforeSubmitFunction();
	}

	var submitButton = $(form).find(':submit');
	if (opts.submitRef) {
		submitButton = $(opts.submitRef);
	}
	if (opts.disableElements) {
		app.deactivateElements(form, submitButton);
	}

	var loader = null;
	if (opts.buttonLoader !== false) {
		loader = spinner.startButtonLoader({
			element: submitButton,
			successText: opts.buttonSuccessText,
			errorText: opts.buttonErrorText,
			success: function() {
				$(form).data('submitting', false);
				if (opts.disableElements) {
					app.activateElements(form);
				}
				if (opts.successFromButton) {
					opts.successFromButton();
				}
			},
			error: function() {
				$(form).data('submitting', false);
				if (opts.disableElements) {
					app.activateElements(form);
				}
				if (opts.errorFromButton) {
					opts.errorFromButton();
				}
			}
		});
	}

	if (opts.showLoader) {
		spinner.showLoader();
	}

	data.performRequest({
		url: $(form).prop('action'),
		data: $(form).serialize(),
		method: $(form).prop('method'),
		dataType: 'json',
		success: function(ret) {
			if (opts.showLoader) {
				spinner.hideLoader();
			}
			if (loader) {
				if (opts.resetButtonLoader) {
					spinner.completeButtonLoader(loader, 'success-reset');
				} else {
					spinner.completeButtonLoader(loader, true);
				}
			} else {
				$(form).data('submitting', false);
				if (opts.disableElements) {
					app.activateElements(form);
				}
			}
			if (opts.success) {
				opts.success(form, ret);
			}
		},
		error: function(ret) {
			if (opts.showLoader) {
				spinner.hideLoader();
			}
			if (loader) {
				if (opts.resetButtonLoader) {
					spinner.completeButtonLoader(loader, 'reset');
				} else {
					spinner.completeButtonLoader(loader, false);
				}
			} else {
				$(form).data('submitting', false);
				if (opts.disableElements) {
					app.activateElements(form);
				}
			}
			if (opts.error) {
				opts.error(form, ret);
			}
		}
	});
}

//PAGE FORM SUBMIT
//adds full page loader, prevents multiple submits, updates section of page with returned html
app.submitPageForm = function(form, opts) {
	if (typeof opts == 'undefined') {
		var opts = {
			replaceRef: '#main-content', 	//reference to be replaced by the json.data response (default: null)
			animate: false					//optionally fade in the new content
		};
	}

	if ($(form).data('submitting') === true) {
		return;
	}
	$(form).data('submitting', true);
	spinner.showLoader();

	data.performRequest({
		url: $(form).prop('action'),
		data: $(form).serialize(),
		method: $(form).prop('method'),
		dataType: 'html',
		success: function(ret) {
			spinner.hideLoader();
			$(form).data('submitting', false);
			if (ret) {
				app.addHTML(ret, opts.replaceRef, opts.animate);
			}
		},
		error: function(ret) {
			spinner.hideLoader();
			$(form).data('submitting', false);
			if (ret) {
				app.addHTML(ret, opts.replaceRef, opts.animate);
			}
		}
	}, false, 'page');
}

//SUBMITS FORM IN MODAL 
//adds button loader, stops other forms, disables/enables elements, adds validation, optionally dismisses on complete, optionally displays confirm box
app.submitModalForm = function(form, opts) {
	if (typeof opts == 'undefined') {
		var opts = {
			submitRef: null, 			//reference to submit button if outside of the form tags (default: null)
			buttonSuccessText: null, 	//button text for when it completes successfully error occurs (defaults to the spinner default)
			buttonErrorText: null,		//button text for when an error occurs (defaults to the spinner default)
			dismiss: true,				//dismiss the modal on success (default: true)
			confirm: null,				//confirm dialog message (default: null)
			error: null,				//callback for error
			success: null,				//callback for success
			successResponse: null		//callback for success with our response as a param (before buttonloader is triggered)
		};
	}
	$('input').blur();

	if (app.modalAjaxBusy) {
		return;
	}
	app.modalAjaxBusy = true;

	//display confirm box
	if (opts.confirm) {
		if (!util.confirm(opts.confirm)) {
			setTimeout(function() { app.modalAjaxBusy = false; }, 250);
			return;
		}
	}

	$(form).find('input, textarea, select').parents().removeClass('has-error');
	$(form).find('#validation-errors').html('');

	//get submit button and add button loader
	var submitButton = $(form).find(':submit');
	if (opts.submitRef) {
		submitButton = $(opts.submitRef);
	}
	var loader = spinner.startButtonLoader({
		element: submitButton,
		successText: opts.buttonSuccessText,
		errorText: opts.buttonErrorText,
		success: function() { 
			if (opts.dismiss !== false) {
				$(form).parents('.modal').modal('hide');
			} else {
				app.activateElements($(form).parents('.modal'));
			}
			setTimeout(function() { app.modalAjaxBusy = false; }, 250);
			if (opts.success) {
				opts.success();
			}
		},
		error: function() {
			app.modalAjaxBusy = false;
			if (opts.error) {
				opts.error();
			}
		}
	});
	if (opts.dismiss === false) {
		loader.resetAfterComplete = true;
	}

	data.performRequest({
		url: $(form).prop('action'),
		data: $(form).serialize(),
		method: $(form).prop('method'),
		dataType: 'json',
		success: function(ret) {
			app.modalDismissJSON = ret;
			if (opts.dismiss !== false) {
				app.modalCloseWarning = false;
			}
			if (opts.successResponse) {
				opts.successResponse(ret);
			}
			spinner.completeButtonLoader(loader, true);
		},
		error: function(ret) {
			if (ret.responseJSON && ret.responseJSON.data) {
				if (ret.status == 404) {
					app.modalCloseWarning = false;
					spinner.completeButtonLoader(loader, false);
					alert('This resource cannot be updated because it no longer exists.')
					app.activateElements($(form).parents('.modal'));
					
				} else if (ret.responseJSON.message == 'Validation error') {

					//validate items
					$(form).find('input, textarea, select').focus(function() {
						$(this).parent().removeClass('has-error');
					}).change(function() {
						$(this).parent().removeClass('has-error');
					});

					var msgCount = 0;
					$.each(ret.responseJSON.data.errors, function(key, value) {
						if (key.indexOf('.') == -1) {
							$(form).find('textarea[name=' + key + '], input[name=' + key + '], input[name=' + key + '_confirmation], select[name=' + key + '], select[name="' + key + '[]"]').parent().addClass('has-error');
						}
						if (value != '') {
						 	msgCount++;
						}
					});
					if (msgCount > 0 && $('#validation-errors').length > 0 && ret.responseJSON.data.html != '') {
						$('#validation-errors').html(ret.responseJSON.data.html);
						if (bowser.ios) {
							if ($(window).scrollTop() > app.modalScrollTop) {
								$(window).scrollTop(app.modalScrollTop);
							}
						} else {
							if ($('.modal').scrollTop() > 0) {
								$('.modal').scrollTop(0);
							}
						}
					}
					spinner.completeButtonLoader(loader, 'reset');
					app.activateElements($(form).parents('.modal'));
				} else {
					spinner.completeButtonLoader(loader, false);
					app.activateElements($(form).parents('.modal'));
				}
			} else {
				spinner.completeButtonLoader(loader, false);
				app.activateElements($(form).parents('.modal'));
			}
		}
	}, false, 'modal');

	//deactivate other forms and elements (must be after ajax submit)
	app.deactivateElements($(form).parents('.modal'), submitButton);
}

//DEACTIVATE FORM ELEMENTS (normally used inside modal)
app.deactivateElements = function(ref, button) {
	$(ref).find('input, textarea, select, button').not('.keep-active, input[type=hidden]').prop('disabled', true);
	$(ref).find('a').not('.keep-active').attr('disabled', true);
	$(button).prop('disabled', false);
}

//ACTIVATE FORM ELEMENTS (again normally used inside a modal after using deactivate)
app.activateElements = function(ref) {
	$(ref).find('input, textarea, select, button').not('.keep-disabled').prop('disabled', false);
	$(ref).find('a').not('.keep-disabled').attr('disabled', false);
}

//SETUP OUR MODAL
app.modalStartup = function() {
	app.applyModalFix();
	helpers.start({ popoverContainer: '.modal' });
	app.modalAjaxBusy = false;
	app.modalHidden = false;
	$('#modal-outer').modal('show');
	app.modalScrollTop = (bowser.ios) ? $(window).scrollTop() : $('.modal').scrollTop();

	$('#modal-outer').on('hide.bs.modal', function (e) {
		if (!e.date && !app.datePickerTimeout) {
			app.onModalDismiss(e, $('#modal-outer').data('dismiss-follow'));
		} else {
			app.datePickerTimeout = true;
			setTimeout(function(){ app.datePickerTimeout = false; }, 100);
			e.preventDefault(); 
		}
	});

	$('#modal-outer').on('hidden.bs.modal', function (e) {
		$('#modal-holder').html('');
		if (app.nextModalHTML) {
			app.addHTML(app.nextModalHTML, '#modal-holder', false);
			spinner.hideLoader();
			app.modalStartup();
			app.nextModalHTML = null;
		} else {
			if (app.loadingModal) {
				spinner.showLoader();
			}
			app.modalHidden = true;
		}
	});

	$('#modal-outer form.close-warning input, #modal-outer form.close-warning textarea, #modal-outer form.close-warning select').change(function() {
		app.modalCloseWarning = true;
	})

	//fix for dual backdrop glitch
	var oldBackdrop = $('.modal-backdrop').first();
	if ($('.modal-backdrop').length > 1 && oldBackdrop) {
		oldBackdrop.transition({ opacity: 0, complete: function() { 
			oldBackdrop.remove();
		}});
	}
}

//APPLIES SCROLL FIXES TO MODALS FOR IOS (MUST BE RUN AFTER CREATING A NEW MODAL) 
app.applyModalFix = function() {
	if (bowser.ios) {
		$('.modal').on('show.bs.modal', function() {
			$(this).css({ position: 'absolute',	marginTop: $(window).scrollTop() + 'px', bottom: 'auto' });
			setTimeout( function() {
				$('.global-modal-backdrop').css({ position: 'absolute', top: 0, left: 0, width: '100%', height: document.body.scrollHeight + 'px' });
			}, 250);
		});
		if (!app.modalWindowResizeApplied) {
			app.modalWindowResizeApplied = true;
			$(window).on('resize', function() {
				$('.global-modal-backdrop').css({ position: 'absolute', top: 0, left: 0, width: '100%', height: document.body.scrollHeight + 'px' });
			});
		}
	}
}

//DISMISS MODAL AND LOAD NEW MODAL URL
app.modalDismiss = function(url) {
	if (!app.confirmModalExit()) {
		return;
	}
	if (app.rememberScrollObj) {
		app.rememberScrollObj.save();
	}
	$('#modal-outer').off('hide.bs.modal');
	$('#modal-outer').modal('hide');
	app.loadModalHTML(url);
}

//ON MODAL DISMISS LOAD OPTIONAL NEW MODAL URL
app.onModalDismiss = function(e, url) {
	if (!app.confirmModalExit() || $(':focus').is('textarea') || $(':focus').is('input')) {
		e.preventDefault();
		e.stopPropagation();
		return false; 
	}
	if (url) {
		app.loadModalHTML(url);
	} else {
		app.globalBackdrop('remove');
	}
	if (typeof(app.modalDismissCallback) === 'function') {
		if (typeof(app.modalDismissJSON) === 'object' && app.modalDismissJSON) {
			if (app.modalDismissJSON.data) {
				app.modalDismissCallback(app.modalDismissJSON.data);
			}
		}
		app.modalDismissJSON = null;
	}
}

//ADD OR REMOVE OUR GLOBAL MODAL BACKDROP
app.globalBackdrop = function(method) {
	if (method == 'add') {
		$('body').append('<div class="global-modal-backdrop"></div>');
		$('.global-modal-backdrop').transition({ opacity: 0.5, duration: 300 });
	} else {
		$('.global-modal-backdrop').transition({ opacity: 0, duration: 300, complete: 
			function() { 
				$('.global-modal-backdrop').remove();
			}
		});
	}
}

//ON MODAL EXIT CONFIRM CHANGES MAY NOT BE SAVED
app.confirmModalExit = function() {
	if (app.modalCloseWarning) {
		return util.confirm('This modal has unsaved changes, are you sure you want to close it?');
	}
	return true;
}

//REMEMBER THE SCROLL POSITION OF A TABLE WITHIN A MODAL
app.rememberScroll = function(obj, ref) {
	app.rememberScrollObj = obj;
	if (obj.scrollPos > 0) {
		setTimeout(function() { 
			$(ref).scrollTop(obj.scrollPos);
			obj.scrollPos = 0;
		}, 250); //delayed to fix issue
	}
	obj.save = function() {
		if ($(ref).length > 0) {
			obj.scrollPos = $(ref).scrollTop();
		}
	}
}

//REMEMBER THE SORT ORDER OF A TABLE SORTER
app.rememberSort = function(obj, ref, headers) {
	if (obj.tableSort == null) {
		obj.tableSort = [];
	}
	if (!headers) {
		headers = { '0': { 'sorter': false }};
	}
	$(ref).tablesorter({
		'sortInitialOrder': 'asc', 
		'headers': headers,
		'sortList': obj.tableSort
	});
	$(ref).on('sortEnd', function(e) { 
		obj.tableSort = e.target.config.sortList;
	});
}

//DISPLAY JS VERSIONS IN THE CONSOLE (NOTE: THIS DOES NOT WORK WHEN COMPILED USING --PRODUCTION)
app.consoleLogVersions = function() {
	console.log('js versions:');
	if (app.version) {
		console.log('app = ' + app.version); 
	}
	if (data && data.version) { 
		console.log('data = ' + data.version); 
	}
	if (helpers && helpers.version) {
		console.log('helpers = ' + helpers.version);
	}
	if (spinner && spinner.version) {
		console.log('spinner = ' + spinner.version);
	}
	if (util && util.version) {
		console.log('util = ' + util.version);
	}
}
