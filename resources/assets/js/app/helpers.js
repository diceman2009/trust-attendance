var helpers = {};

helpers.version = 1.01;

//USED TO ADD EVENTS AND FLAT UI BITS ACROSS THE SYSTEM
helpers.start = function(items, clearDefaults) {
	var allItems = {};
	if (!clearDefaults) {
		allItems = { 'select': true, 'checkbox': true, 'radio': true, 'switches': true, 'tooltip': true, 'inputs': true, 'popover': true, 'maxlength': true };
	}
	for (var property in items) {
	    if (items.hasOwnProperty(property)) {
	    	allItems[property] = items[property];
	    }
	}
	items = allItems;
	
	var container = (items.container) ? items.container : '';

	$('.select2-hidden-accessible, .select2-drop-mask, .select2-drop').remove();
	if (items.select) { $('select').select2({ 'minimumResultsForSearch': -1, dropdownCssClass: 'select-inverse-dropdown' }); }
	
	if (items.checkbox) { $('[data-toggle="checkbox"]').radiocheck(); }
	if (items.radio) { $('[data-toggle="radio"]').radiocheck(); }
	if (items.tooltip) { $(container + '[data-toggle=tooltip]').tooltip('show'); }
	if (items.switches) { if ($(container + '[data-toggle="switch"]').length) { $(container + '[data-toggle="switch"]').bootstrapSwitch(); } }
	
	if (items.popover) { 
		if (items.popoverContainer) {
			$(container + ' [data-toggle="popover"]').popover({ container: items.popoverContainer });
		} else {
			$(container + ' [data-toggle="popover"]').popover();
		}
	}

	//focus state for append/prepend inputs
	if (items.inputs) {
		$('.input-group').on('focus', '.form-control', function () {
			$(this).closest('.input-group, .form-group').addClass('focus');
		}).on('blur', '.form-control', function () {
			$(this).closest('.input-group, .form-group').removeClass('focus');
		});

		$('.btn-group').on('click', 'a', function () {
			$(this).siblings().removeClass('active').end().addClass('active');
		});
	}

	//maxlengths
	if (items.maxlength) {
		$(container + ' .maxlength-with-button input[maxlength]').maxlength({
			placement: 'bottom',
			warningClass: 'label label-sm label-primary label-maxlength label-maxlength-button',
			limitReachedClass: 'label label-sm label-danger label-maxlength label-maxlength-button'
		});
		$(container + ' .maxlength-no-button input[maxlength]').maxlength({
			placement: 'bottom',
			warningClass: 'label label-sm label-primary label-maxlength',
			limitReachedClass: 'label label-sm label-danger label-maxlength'
		});
		$(container + ' .maxlength-with-button textarea[maxlength]').maxlength({
			placement: 'bottom',
			warningClass: 'label label-sm label-primary label-maxlength label-maxlength-button',
			limitReachedClass: 'label label-sm label-danger label-maxlength label-maxlength-button'
		});
		$(container + ' .maxlength-no-button textarea[maxlength]').maxlength({
			placement: 'bottom',
			warningClass: 'label label-sm label-primary label-maxlength',
			limitReachedClass: 'label label-sm label-danger label-maxlength'
		});
	}
}