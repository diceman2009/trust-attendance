var data = {};

data.version = 1.01;
data.requestTally = 0;
data.activeRequests = [];

$(document).ready(function() {
	//DISABLE CACHING ON ALL AJAX REQUESTS (MAINLY FOR IE) AND ADDS CSRF TOKEN
	$.ajaxSetup({ 
		cache: false,
		headers: { 
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
});

//PERFORM REQUEST USING AN AJAX OBJECT, STORE IT IN AN ARRAY TO ALLOW FOR MULTIPLE AT ONCE
data.performRequest = function(obj, dontStore, category) {
	if (!dontStore) {
		data.requestTally++;
		var tally = data.requestTally;
		var reqBundle = {
			'id': tally, 
			'success': obj.success, 
			'error': obj.error, 
			'category': category, 
			'retries': 0, 
			'original': obj 
		};
		obj.success = function(ret) { data.requestSuccess(tally, ret); };
		obj.error = function(ret) { data.requestFail(tally, ret); };
		reqBundle.ajax = $.ajax(obj);
		data.activeRequests.push(reqBundle);
	} else {
		$.ajax(obj);
	}
}

//ON SUCCESS REMOVE FROM ACTIVE AND TRIGGER AJAX SUCCESS FUNCTION
data.requestSuccess = function(id, ret) {
	var loc = data.getIndexByName(data.activeRequests, id, 'id');
	if (loc > -1) {
		var item = data.activeRequests.splice(loc, 1)[0];
		if (item.success) { item.success(ret); }
	}
}

//ON FAIL REMOVE FROM ACTIVE AND TRIGGER AJAX ERROR FUNCTION
data.requestFail = function(id, ret) {
	var loc = data.getIndexByName(data.activeRequests, id, 'id');
	if (loc > -1) {
		if (typeof(ret) == 'object') {
			if (ret.status == 401) {
				app.modalCloseWarning = false;
				location.reload(); //refresh to display login page
			} else if (!ret.responseJSON && ret.responseText == '' && ret.status == 0 && ret.statusText !== 'abort') {
				if (data.activeRequests[loc].retries < 1) {
					data.activeRequests[loc].retries++;
					data.activeRequests[loc].ajax = $.ajax(data.activeRequests[loc].original);
					return;
				}
				alert('Device offline, please connect to the internet and try again.'); //your offline so alert message
			}
		}
		var item = data.activeRequests.splice(loc, 1)[0];
		if (item.error) { item.error(ret); }
	}
}

//CANCEL ALL OF OUR ACTIVE DATA REQUESTS SILENTLY (ERROR FUNCTIONS WILL NOT BE CALLED)
data.cancelAllRequests = function(category) {
	for (var x = data.activeRequests.length-1; x >= 0; x--) {
		if (!category || (!data.activeRequests[x].category || data.activeRequests[x].category == category)) {
			data.activeRequests.splice(x, 1)[0].ajax.abort();
		}
	}
}

/**
 * Returns the id of an item in an array
 * @param a Array to be searched
 * @param s Search term
 * @param p1 If the item is in a property send the name of the property (p2 and p3 are deeper properties)
 * @return
 */
data.getIndexByName = function(a, s, p1, p2, p3) {
	if (!a) { return -1; }
	p1 = typeof p1 !== 'undefined' ? p1 : '';
	p2 = typeof p2 !== 'undefined' ? p2 : '';
	p3 = typeof p3 !== 'undefined' ? p3 : '';
	for (var i = 0; i < a.length; i++) {
		if (p1 != '' && p2 != '' && p3 != '') {
			if (a[i][p1][p2][p3] == s) { return i; }
		} else if (p1 != '' && p2 != '') {
			if (a[i][p1][p2] == s) { return i; }
		} else if (p1 != '') {
			if (a[i][p1] == s) { return i; }
		} else {
			if (a[i] == s) { return i; }
		}
	}
	return -1;
}