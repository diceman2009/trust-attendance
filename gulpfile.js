var elixir = require('laravel-elixir');

elixir(function(mix) {

	// copy additional style sheets
	mix.styles('vendor/bootstrap.min.css', 'public/css/bootstrap.css');
	mix.styles('vendor/flat-ui.css', 'public/css/flat-ui.css');
	mix.styles('vendor/datepicker3.min.css', 'public/css/datepicker3.css');
	mix.styles('custom.css', 'public/css/custom.css');
	mix.styles('ie8tweaks.css', 'public/css/ie8tweaks.css');
	mix.styles('ietweaks.css', 'public/css/ietweaks.css');
	mix.styles('print.css', 'public/css/print.css');
	mix.styles('trust.css', 'public/css/trust.css');

	// merge main js
	mix.scripts([
		'vendor/jquery-1.12.3.min.js',
		'vendor/flat-ui.js',
		'vendor/jquery.transit-0.9.12.min.js',
		'vendor/bowser.min.js',
		'vendor/json2.min.js',
		'vendor/bootstrap-datepicker.min.js',
		'vendor/bootstrap-maxlength.min.js',
		'vendor/jquery.tablesorter.min.js',
		'vendor/ie10-viewport-bug-workaround.js',
		'app/spinner.js',
		'app/data.js',
		'app/app.js',
		'app/helpers.js',
		'app/util.js'
	], 'public/js/main.js');

	// anti cache
	mix.version([ 
		'css/custom.css',
		'css/ie8tweaks.css',
		'css/ietweaks.css',
		'css/print.css',
		'css/trust.css',
		'js/main.js',
	]);

});